import { AuthService } from 'src/auth/services/auth.service';
import { SprintService } from 'src/sprint/services/sprint.service';
import { TokenService } from 'src/auth/services/token.service';
import { RoleService } from 'src/role/services/role.service';
import { TaskService } from 'src/task/services/task.service';
import { UserService } from 'src/user/services/user.service';
import { BacklogService } from '../backlog/services/backlog.service';
import { ProjectService } from '../project/services/project.service';
import { UserProjectService } from 'src/project/services/user-project.service';
import { InviteService } from '../invite/services/invite.service';
import { InviteCoherenceService } from '../invite/services/invite-coherence.service';
import { MailService } from 'src/mail/services/mail.service';
import { PinoLoggerService } from '../logger/services/pino-logger.service';

export const diUserService = {
  provide: 'USER_SERVICE',
  useClass: UserService,
};

export const diRoleService = {
  provide: 'ROLE_SERVICE',
  useClass: RoleService,
};

export const diAauthService = {
  provide: 'AUTH_SERVICE',
  useClass: AuthService,
};

export const diSprintService = {
  provide: 'SPRINT_SERVICE',
  useClass: SprintService,
};

export const diTokenService = {
  provide: 'TOKEN_SERVICE',
  useClass: TokenService,
};

export const diTaskService = {
  provide: 'TASK_SERVICE',
  useClass: TaskService,
};

export const diProjectService = {
  provide: 'PROJECT_SERVICE',
  useClass: ProjectService,
};

export const diBacklogService = {
  provide: 'BACKLOG_SERVICE',
  useClass: BacklogService,
};

export const diUserProjectService = {
  provide: 'USER_PROJECT_SERVICE',
  useClass: UserProjectService,
};

export const diInviteService = {
  provide: 'INVITE_SERVICE',
  useClass: InviteService,
};

export const diInviteCoherenceService = {
  provide: 'INVITES_USERS_ROLES_PROJECTS_COHERENCE_SERVICE',
  useClass: InviteCoherenceService,
};

export const diMailService = {
  provide: 'MAIL_SERVICE',
  useClass: MailService,
};

export const diLoggerService = {
  provide: 'PINO_LOGGER_SERVICE',
  useClass: PinoLoggerService,
};
