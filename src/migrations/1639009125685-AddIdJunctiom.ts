import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddIdJunctiom1639009125685 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE user_project_junction
                DROP CONSTRAINT user_project_junction_pkey;
            ALTER TABLE "user_project_junction" 
                ADD "id" SERIAL  PRIMARY KEY;

            ALTER TABLE user_project_junction
                DROP CONSTRAINT user_project_junction_project_id_fkey;
            ALTER TABLE user_project_junction
                ADD CONSTRAINT user_project_junction_project_id_fkey
                FOREIGN KEY (project_id)
                REFERENCES projects (id)
                ON DELETE CASCADE;

            ALTER TABLE user_project_junction
                DROP CONSTRAINT user_project_junction_user_id_fkey;
            ALTER TABLE user_project_junction
                ADD CONSTRAINT user_project_junction_user_id_fkey
                FOREIGN KEY (user_id)
                REFERENCES users (id)
                ON DELETE CASCADE;
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE user_project_junction
                DROP CONSTRAINT user_project_junction_pkey;
            ALTER TABLE "user_project_junction" 
                DROP COLUMN id;
            ALTER TABLE "user_project_junction" 
                ADD PRIMARY KEY ("project_id", "user_id");

            ALTER TABLE user_project_junction
                DROP CONSTRAINT user_project_junction_project_id_fkey;
            ALTER TABLE user_project_junction
                ADD CONSTRAINT user_project_junction_project_id_fkey
                FOREIGN KEY (project_id)
                REFERENCES projects (id);

            ALTER TABLE user_project_junction
                DROP CONSTRAINT user_project_junction_user_id_fkey;
            ALTER TABLE user_project_junction
                ADD CONSTRAINT user_project_junction_user_id_fkey
                FOREIGN KEY (user_id)
                REFERENCES users (id);
        `);
  }
}
