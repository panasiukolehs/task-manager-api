import { MigrationInterface, QueryRunner } from 'typeorm';

export class addRoles1640023134442 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(
      `INSERT INTO "roles" ("role_name") VALUES ('Project Manager'), ('Developer'), ('Super Admin')`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(`TRUNCATE TABLE "roles" CASCADE`);
  }
}
