import { MigrationInterface, QueryRunner } from 'typeorm';

export class alterUserContraintInTokenTable1638892693873 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "refresh_tokens" DROP CONSTRAINT "refresh_tokens_user_id_fkey";` +
        `ALTER TABLE "refresh_tokens" ADD CONSTRAINT "refresh_tokens_user_id_fkey"` +
        `FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON DELETE CASCADE;`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "refresh_tokens" DROP CONSTRAINT "refresh_tokens_user_id_fkey";` +
        `ALTER TABLE "refresh_tokens" ADD CONSTRAINT "refresh_tokens_user_id_fkey"` +
        `FOREIGN KEY ("user_id") REFERENCES "users" ("id");`,
    );
  }
}
