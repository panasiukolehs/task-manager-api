import { MigrationInterface, QueryRunner } from 'typeorm';

export class updateForeignKeyInTasksTable1639233527459 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "tasks" DROP CONSTRAINT "tasks_user_id_fkey";` +
        `ALTER TABLE "tasks" ADD CONSTRAINT "tasks_user_id_fkey" ` +
        `FOREIGN KEY (user_id) REFERENCES "users" (id) ON DELETE SET NULL ON UPDATE NO ACTION;`,
    );
    await queryRunner.query(
      `ALTER TABLE "tasks" DROP CONSTRAINT "tasks_sprint_id_fkey";` +
        `ALTER TABLE "tasks" ADD CONSTRAINT "tasks_sprint_id_fkey" ` +
        `FOREIGN KEY (sprint_id) REFERENCES "sprints" (id) ON DELETE CASCADE ON UPDATE NO ACTION;`,
    );
    await queryRunner.query(
      `ALTER TABLE "tasks" DROP CONSTRAINT "tasks_backlog_id_fkey";` +
        `ALTER TABLE "tasks" ADD CONSTRAINT "tasks_backlog_id_fkey" ` +
        `FOREIGN KEY (backlog_id) REFERENCES "backlogs" (id) ON DELETE CASCADE ON UPDATE NO ACTION;`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "tasks" DROP CONSTRAINT "tasks_user_id_fkey";` +
        `ALTER TABLE "tasks" ADD CONSTRAINT "tasks_user_id_fkey" ` +
        `FOREIGN KEY (user_id) REFERENCES "users" (id);`,
    );
    await queryRunner.query(
      `ALTER TABLE "tasks" DROP CONSTRAINT "tasks_sprint_id_fkey";` +
        `ALTER TABLE "tasks" ADD CONSTRAINT "tasks_sprint_id_fkey" ` +
        `FOREIGN KEY (sprint_id) REFERENCES "sprints" (id);`,
    );
    await queryRunner.query(
      `ALTER TABLE "tasks" DROP CONSTRAINT "tasks_backlog_id_fkey";` +
        `ALTER TABLE "tasks" ADD CONSTRAINT "tasks_backlog_id_fkey" ` +
        `FOREIGN KEY (backlog_id) REFERENCES "backlogs" (id);`,
    );
  }
}
