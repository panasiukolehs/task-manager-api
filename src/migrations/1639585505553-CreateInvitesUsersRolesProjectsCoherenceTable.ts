import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateInvitesUsersRolesProjectsCoherenceTable1639585505553
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            CREATE TABLE "invites_users_roles_projects_coherence" (
                "id" SERIAL PRIMARY KEY, 
                "userId" integer, 
                "inviteId" integer NOT NULL, 
                "projectId" integer NOT NULL, 
                "roleId" integer NOT NULL, 
                "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP, 
                "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
                "expires_at" TIMESTAMP NOT NULL
            );`);
    await queryRunner.query(`
            ALTER TABLE "invites_users_roles_projects_coherence" 
                ADD CONSTRAINT "FK_e98e8305d5f6c7c00400c301dde" 
                FOREIGN KEY ("userId") 
                REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    await queryRunner.query(`
            ALTER TABLE "invites_users_roles_projects_coherence" 
                ADD CONSTRAINT "FK_c8d0645d57ad4ce458383694750" 
                FOREIGN KEY ("inviteId") 
                REFERENCES "invites"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    await queryRunner.query(`
            ALTER TABLE "invites_users_roles_projects_coherence" 
                ADD CONSTRAINT "FK_4b77c780073073e3d5a90378992" 
                FOREIGN KEY ("projectId") 
                REFERENCES "projects"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    await queryRunner.query(`
            ALTER TABLE "invites_users_roles_projects_coherence" 
                ADD CONSTRAINT "FK_593f76e146ca64a1820d81e038e" 
                FOREIGN KEY ("roleId") 
                REFERENCES "roles"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    await queryRunner.query(`
            ALTER TABLE "invites_users_roles_projects_coherence"
                ADD CONSTRAINT "unique_invite" UNIQUE ("inviteId")`);
    await queryRunner.query(`
            CREATE TRIGGER "updated_at_invites_users_roles_projects_coherence" BEFORE UPDATE
                ON "invites_users_roles_projects_coherence" FOR EACH ROW EXECUTE PROCEDURE
                update_changetimestamp_column();
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            DROP TRIGGER "updated_at_invites_users_roles_projects_coherence" 
            ON "invites_users_roles_projects_coherence";
        `);
    await queryRunner.query(`
            ALTER TABLE "invites_users_roles_projects_coherence"
                DROP CONSTRAINT "unique_invite"`);
    await queryRunner.query(`
            ALTER TABLE "invites_users_roles_projects_coherence" 
                DROP CONSTRAINT "FK_593f76e146ca64a1820d81e038e"`);
    await queryRunner.query(`
            ALTER TABLE "invites_users_roles_projects_coherence" 
                DROP CONSTRAINT "FK_4b77c780073073e3d5a90378992"`);
    await queryRunner.query(`
            ALTER TABLE "invites_users_roles_projects_coherence" 
                DROP CONSTRAINT "FK_c8d0645d57ad4ce458383694750"`);
    await queryRunner.query(`
            ALTER TABLE "invites_users_roles_projects_coherence" 
                DROP CONSTRAINT "FK_e98e8305d5f6c7c00400c301dde"`);
    await queryRunner.query(`
            DROP TABLE "invites_users_roles_projects_coherence"`);
  }
}
