import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateSprintsTable1639061330422 implements MigrationInterface {
  name = 'UpdateSprintsTable1639061330422';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "sprints" DROP CONSTRAINT "sprints_project_id_fkey"`);
    await queryRunner.query(`ALTER TABLE "sprints" DROP COLUMN "project_id"`);
    await queryRunner.query(`ALTER TABLE "sprints" ADD "date_of_period" TIMESTAMP NOT NULL`);
    await queryRunner.query(`ALTER TABLE "sprints" ADD "projectId" integer`);
    await queryRunner.query(`ALTER TABLE "sprints" ALTER COLUMN "name" SET NOT NULL`);
    await queryRunner.query(`ALTER TABLE "sprints" DROP COLUMN "status"`);
    await queryRunner.query(`ALTER TABLE "sprints" ADD "status" boolean NOT NULL DEFAULT true`);
    await queryRunner.query(
      `ALTER TABLE "sprints" ADD CONSTRAINT "FK_f60645f2ec0c86773d956bea273" FOREIGN KEY ("projectId") REFERENCES "projects"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "sprints" DROP CONSTRAINT "FK_f60645f2ec0c86773d956bea273"`,
    );
    await queryRunner.query(`ALTER TABLE "sprints" DROP COLUMN "status"`);
    await queryRunner.query(`ALTER TABLE "sprints" ADD "status" character varying`);
    await queryRunner.query(`ALTER TABLE "sprints" ALTER COLUMN "name" DROP NOT NULL`);
    await queryRunner.query(`ALTER TABLE "sprints" DROP COLUMN "projectId"`);
    await queryRunner.query(`ALTER TABLE "sprints" DROP COLUMN "date_of_period"`);
    await queryRunner.query(`ALTER TABLE "sprints" ADD "project_id" integer`);
    await queryRunner.query(
      `ALTER TABLE "sprints" ADD CONSTRAINT "sprints_project_id_fkey" FOREIGN KEY ("project_id") REFERENCES "projects"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
