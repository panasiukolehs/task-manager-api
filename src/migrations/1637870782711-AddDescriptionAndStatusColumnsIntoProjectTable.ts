import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddDescriptionAndStatusColumnsIntoProjectTable1637870782711
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        ALTER TABLE projects
            ADD description VARCHAR(255) DEFAULT '';
        ALTER TABLE projects
            ADD status BOOLEAN DEFAULT TRUE;
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        ALTER TABLE projects
            DROP description;
        ALTER TABLE projects
            DROP status;
    `);
  }
}
