import { MigrationInterface, QueryRunner } from 'typeorm';

export class addAuthorColumnToTasksTable1639063019567 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "tasks" ADD "author_id" INT NOT NULL;` +
        `ALTER TABLE "tasks" ` +
        `ADD CONSTRAINT "author_id_fkey" FOREIGN KEY (author_id) REFERENCES "users" (id) ON DELETE CASCADE;`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "tasks" DROP CONSTRAINT "author_id_fkey";` +
        `ALTER TABLE "tasks" DROP COLUMN "author_id";`,
    );
  }
}
