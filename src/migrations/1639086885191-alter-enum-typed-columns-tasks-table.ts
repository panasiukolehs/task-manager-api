import { MigrationInterface, QueryRunner } from 'typeorm';

export class alterEnumTypedColumnsTasksTable1639086885191 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "tasks" ALTER COLUMN "priority" SET DEFAULT 'medium', ALTER COLUMN "priority" SET NOT NULL;` +
        `ALTER TABLE "tasks" ALTER COLUMN "progress" SET DEFAULT 'todo', ALTER COLUMN "progress" SET NOT NULL;` +
        `ALTER TABLE "tasks" ALTER COLUMN "type" SET DEFAULT 'task', ALTER COLUMN "type" SET NOT NULL;`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "tasks" ALTER COLUMN "priority" DROP DEFAULT, ALTER COLUMN "priority" DROP NOT NULL;` +
        `ALTER TABLE "tasks" ALTER COLUMN "progress" DROP DEFAULT, ALTER COLUMN "progress" DROP NOT NULL;` +
        `ALTER TABLE "tasks" ALTER COLUMN "type" DROP DEFAULT, ALTER COLUMN "type" DROP NOT NULL;`,
    );
  }
}
