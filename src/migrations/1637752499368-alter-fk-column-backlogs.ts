import { MigrationInterface, QueryRunner } from 'typeorm';

export class alterFkColumnBacklogs1637752499368 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "backlogs" ADD CONSTRAINT "UQ_project_id" UNIQUE ("project_id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "backlogs" DROP CONSTRAINT "UQ_project_id"`);
  }
}
