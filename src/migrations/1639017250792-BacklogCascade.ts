import { MigrationInterface, QueryRunner } from 'typeorm';

export class BacklogCascade1639017250792 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE backlogs
                DROP CONSTRAINT backlogs_project_id_fkey;
            ALTER TABLE backlogs
                ADD CONSTRAINT backlogs_project_id_fkey
                FOREIGN KEY (project_id)
                REFERENCES projects (id)
                ON DELETE CASCADE;
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE backlogs
                DROP CONSTRAINT backlogs_project_id_fkey;
            ALTER TABLE backlogs
                ADD CONSTRAINT backlogs_project_id_fkey
                FOREIGN KEY (project_id)
                REFERENCES projects (id);
        `);
  }
}
