import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateInvitesTable1639568058342 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            CREATE TABLE "invites" (
                "id" SERIAL PRIMARY KEY,
                "invite" uuid NOT NULL DEFAULT uuid_generate_v4(),
                "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
                "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
                "expires_at" TIMESTAMP NOT NULL                
            );
        `);
    await queryRunner.query(`
            CREATE TRIGGER "updated_at_invites" BEFORE UPDATE
                ON "invites" FOR EACH ROW EXECUTE PROCEDURE
                update_changetimestamp_column();
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TRIGGER "updated_at_invites" ON "invites";`);
    await queryRunner.query(`DROP TABLE "invites";`);
  }
}
