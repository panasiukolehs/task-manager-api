import { MigrationInterface, QueryRunner } from 'typeorm';

export class addEstimatedTimeColumnToTasksTable1639136400136 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "tasks" ADD "estimated_time_ms" INT`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "tasks" DROP COLUMN "estimated_time_ms"`);
  }
}
