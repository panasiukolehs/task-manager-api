import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { InviteEntity } from './entities/invite.entity';
import { InviteController } from './controllers/invite.controller';
import {
  diBacklogService,
  diInviteCoherenceService,
  diInviteService,
  diMailService,
  diProjectService,
  diRoleService,
  diUserProjectService,
  diUserService,
  diLoggerService,
} from '../DIConfig/serviceConfig';
import {
  backlogRepositoryDAO,
  inviteCoherenceRepositoryDAO,
  inviteRepositoryDAO,
  projectRepositoryDAO,
  roleRepositoryDAO,
  userProjectJucntionRepositoryDAO,
  userRepositoryDAO,
} from '../shared/dao/ConnectionDAO';
import { User } from '../user/entities/user.entity';
import { Role } from '../role/entities/role.entity';
import { EncodingHepler } from '../shared/encoding/encodingHelper';
import { InvitesUsersRolesProjectsCoherenceEntity } from './entities/invites-users-roles-projects-coherence.entity';
import { ProjectEntity } from '../project/entities/project.entity';
import { Backlog } from '../backlog/entities/backlog.entity';
import { UserProjectJunction } from '../project/entities/userProjectJunction.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      InviteEntity,
      InvitesUsersRolesProjectsCoherenceEntity,
      User,
      Role,
      ProjectEntity,
      Backlog,
      UserProjectJunction,
    ]),
  ],
  controllers: [InviteController],
  providers: [
    diInviteService,
    diUserService,
    diRoleService,
    diProjectService,
    diBacklogService,
    diUserProjectService,
    diInviteCoherenceService,
    diMailService,
    diLoggerService,
    inviteRepositoryDAO,
    userRepositoryDAO,
    roleRepositoryDAO,
    projectRepositoryDAO,
    userProjectJucntionRepositoryDAO,
    backlogRepositoryDAO,
    inviteCoherenceRepositoryDAO,
    EncodingHepler,
  ],
  exports: [
    diInviteService,
    diInviteCoherenceService,
    inviteRepositoryDAO,
    inviteCoherenceRepositoryDAO,
  ],
})
export class InviteModule {}
