import { IsEmail, IsInt, IsNotEmpty, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

import { IInviteCreate } from '../interfaces/invite-create.interface';

export class CreateInviteDto implements IInviteCreate {
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  @IsInt()
  projectId: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  @IsInt()
  roleId: number;
}
