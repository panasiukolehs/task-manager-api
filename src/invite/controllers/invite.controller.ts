import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Post,
  Req,
  UseFilters,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiGoneResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';

import { CreateInviteDto } from '../dto/createInvite.dto';
import { IInviteService } from '../interfaces/invite-service.interface';
import { INVITE_SERVICE } from '../../constants/serviceConstants';
import { CreateUserDto } from '../../user/dto/create-user.dto';
import { SingleErrorDto } from '../../shared/swagger/dto/single-error.dto';
import { Roles } from '../../role-based-auth/decorators/role.decorator';
import { PROJECT_MANAGER } from '../../constants/roleConstants';
import { RolesGuard } from '../../role-based-auth/roles.guard';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { InviteActivateFilter } from '../filters/invite-activate.filter';

@ApiTags('invites')
@Controller()
export class InviteController {
  constructor(
    @Inject(INVITE_SERVICE)
    private readonly inviteService: IInviteService,
  ) {}

  @ApiOkResponse({
    description: 'User was subscribed to a project',
    type: String,
  })
  @ApiBadRequestResponse({
    description: 'Invalid UUID format',
    type: SingleErrorDto,
  })
  @ApiNotFoundResponse({
    description: "Invite with current UUID doesn't exist",
    type: SingleErrorDto,
  })
  @ApiGoneResponse({
    description: 'Invite with current UUID was canceled',
    type: SingleErrorDto,
  })
  @ApiUnprocessableEntityResponse({
    description: "User with current email isn't registered in the system",
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Get('invitation/:inviteUUID')
  @UseFilters(InviteActivateFilter)
  async inviteResolver(@Param('inviteUUID') inviteUuid: string): Promise<string> {
    await this.inviteService.inviteResolver(inviteUuid);
    return 'You are successfully subscribed to a project';
  }

  @ApiCreatedResponse({
    description: 'User was registered and subscribed to a project',
    type: String,
  })
  @ApiBadRequestResponse({
    description: 'Invalid UUID format',
    type: SingleErrorDto,
  })
  @ApiNotFoundResponse({
    description: "Invite with current UUID doesn't exist",
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Post('invitation/:inviteUUID')
  async registerUserAndSubscribe(
    @Body() createUser: CreateUserDto,
    @Param('inviteUUID') inviteUuid: string,
  ): Promise<string> {
    await this.inviteService.createUserAndSubscribe(inviteUuid, createUser);
    return 'You are successfully registered and subscribed to a project';
  }

  @ApiBearerAuth()
  @ApiCreatedResponse({
    description: 'Invite was created and sent by email',
    type: String,
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthenticated access',
    type: SingleErrorDto,
  })
  @ApiForbiddenResponse({
    description: 'Unauthorized access',
    type: SingleErrorDto,
  })
  @ApiNotFoundResponse({
    description: "Project or role with current ID doesn't exist",
    type: SingleErrorDto,
  })
  @ApiUnprocessableEntityResponse({
    description: '"Super Admin" role can\'t be set by invitation',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Roles(PROJECT_MANAGER)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post('invite')
  async createAndSendInvite(
    @Body() createInvite: CreateInviteDto,
    @Req() req: Request,
  ): Promise<string> {
    await this.inviteService.createAndSendInvite(createInvite, req.protocol);
    return 'Invitation link was successfully created and sent';
  }
}
