import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  UnprocessableEntityException,
} from '@nestjs/common';
import { Response, Request } from 'express';

@Catch(UnprocessableEntityException)
export class InviteActivateFilter implements ExceptionFilter {
  catch(exception: UnprocessableEntityException, host: ArgumentsHost): void {
    const response = host.switchToHttp().getResponse<Response>();
    const request = host.switchToHttp().getRequest<Request>();
    response.redirect(`${process.env.REGISTRATION_BY_INVITE_LINK}${request.params['inviteUUID']}`);
  }
}
