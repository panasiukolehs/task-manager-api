import { Column, Entity, Generated, PrimaryGeneratedColumn } from 'typeorm';

@Entity('invites')
export class InviteEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Generated('uuid')
  invite: string;

  @Column({ name: 'created_at', select: false })
  createdAt?: Date;

  @Column({ name: 'updated_at', select: false })
  updatedAt?: Date;

  @Column({ name: 'expires_at' })
  expiresAt: Date;
}
