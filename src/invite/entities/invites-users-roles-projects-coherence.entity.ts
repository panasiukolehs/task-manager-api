import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

import { User as UserEntity } from '../../user/entities/user.entity';
import { InviteEntity } from './invite.entity';
import { ProjectEntity } from '../../project/entities/project.entity';
import { Role as RoleEntity } from '../../role/entities/role.entity';

@Entity('invites_users_roles_projects_coherence')
export class InvitesUsersRolesProjectsCoherenceEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'int' })
  userId: number;

  @Column({ type: 'int', nullable: false, unique: true })
  inviteId: number;

  @Column({ type: 'int', nullable: false })
  projectId: number;

  @Column({ type: 'int', nullable: false })
  roleId: number;

  @ManyToOne(() => UserEntity, { eager: true })
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  user?: UserEntity;

  @OneToOne(() => InviteEntity, { eager: true })
  @JoinColumn({ name: 'inviteId', referencedColumnName: 'id' })
  invite?: InviteEntity;

  @ManyToOne(() => ProjectEntity, { eager: true })
  @JoinColumn({ name: 'projectId', referencedColumnName: 'id' })
  project?: ProjectEntity;

  @ManyToOne(() => RoleEntity, { eager: true })
  @JoinColumn({ name: 'roleId', referencedColumnName: 'id' })
  role?: RoleEntity;

  @Column({ name: 'created_at', select: false })
  createdAt?: Date;

  @Column({ name: 'updated_at', select: false })
  updatedAt?: Date;

  @Column({ name: 'expires_at' })
  expiresAt: Date;
}
