import {
  Inject,
  Injectable,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';

import { RepositoryTypes } from '../../shared/dao/types/repository-types.enum';
import { inviteCoherenceRepositoryType } from '../../shared/dao/ConnectionDAO';
import { IInvitesUsersRolesProjectsCoherence } from '../interfaces/invites-users-roles-projects-coherence.interface';
import { IInviteCoherenceCreate } from '../interfaces/invite-coherence-create.interface';
import {
  PINO_LOGGER_SERVICE,
  PROJECT_SERVICE,
  ROLE_SERVICE,
} from '../../constants/serviceConstants';
import { IProjectsService } from '../../project/interfaces/IProjectService';
import { IRoleService } from '../../role/interfaces/IRoleService';
import { IInviteCoherenceService } from '../interfaces/invite-coherence-service.interface';
import { PinoLoggerService } from '../../logger/services/pino-logger.service';
import { SUPER_ADMIN } from '../../constants/roleConstants';

@Injectable()
export class InviteCoherenceService implements IInviteCoherenceService {
  constructor(
    @Inject(RepositoryTypes.INVITE_COHERENCE_REPOSITORY)
    private readonly inviteCoherenceRepository: inviteCoherenceRepositoryType,
    @Inject(PROJECT_SERVICE)
    private readonly projectService: IProjectsService,
    @Inject(ROLE_SERVICE)
    private readonly roleService: IRoleService,
    @Inject(PINO_LOGGER_SERVICE)
    private readonly logger: PinoLoggerService,
  ) {
    logger.setContext('InviteCoherenceService');
  }

  async findCoherenceByInviteId(inviteId: number): Promise<IInvitesUsersRolesProjectsCoherence> {
    return await this.inviteCoherenceRepository.findCoherenceByInviteId(inviteId);
  }

  async createInviteCoherence(
    createCoherence: IInviteCoherenceCreate,
  ): Promise<IInvitesUsersRolesProjectsCoherence> {
    const isProject = await this.projectService.getProjectById(createCoherence.projectId);
    if (!isProject) {
      this.logger.warn(`The project with ID=${createCoherence.projectId} doesn't exist`);
      throw new NotFoundException(`The project with ID=${createCoherence.projectId} doesn't exist`);
    }
    const isRole = await this.roleService.getRoleById(createCoherence.roleId);
    if (!isRole) {
      this.logger.warn(`The role with ID=${createCoherence.roleId} doesn't exist`);
      throw new NotFoundException(`The role with ID=${createCoherence.roleId} doesn't exist`);
    }
    if (isRole.roleName === SUPER_ADMIN) {
      this.logger.warn("This role can't be set by invitation");
      throw new UnprocessableEntityException("This role can't be set by invitation");
    }
    return await this.inviteCoherenceRepository.createInviteCoherence(createCoherence);
  }
}
