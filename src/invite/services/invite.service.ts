import {
  BadRequestException,
  GoneException,
  Inject,
  Injectable,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';

import { generateExpiresDate } from '../../shared/functions/generateExpiresDate';
import { generateInviteLink } from '../../shared/functions/generateInviteLink';
import { isValidUUID } from '../../shared/functions/isValidUUID';
import { RepositoryTypes } from '../../shared/dao/types/repository-types.enum';
import { inviteRepositoryType } from '../../shared/dao/ConnectionDAO';
import { IInviteCreate } from '../interfaces/invite-create.interface';
import { IInvite } from '../interfaces/invite.interface';
import {
  INVITE_COHERENCE_SERVICE,
  MAIL_SERVICE,
  PINO_LOGGER_SERVICE,
  USER_PROJECT_SERVICE,
  USER_SERVICE,
} from '../../constants/serviceConstants';
import { IInviteService } from '../interfaces/invite-service.interface';
import { IInvitesUsersRolesProjectsCoherence } from '../interfaces/invites-users-roles-projects-coherence.interface';
import { IUserProjectService } from '../../project/interfaces/IUserProjectService';
import { IUserService } from '../../user/interfaces/IUserService';
import { IInviteCoherenceService } from '../interfaces/invite-coherence-service.interface';
import { ICreateUser } from '../../user/interfaces/ICreateUser';
import { IMailService } from '../../mail/interfaces/IMailService';
import { PinoLoggerService } from '../../logger/services/pino-logger.service';

@Injectable()
export class InviteService implements IInviteService {
  constructor(
    @Inject(RepositoryTypes.INVITE_REPOSITORY)
    private readonly inviteRepository: inviteRepositoryType,
    @Inject(USER_SERVICE)
    private readonly userService: IUserService,
    @Inject(INVITE_COHERENCE_SERVICE)
    private readonly inviteCoherenceService: IInviteCoherenceService,
    @Inject(USER_PROJECT_SERVICE)
    private readonly userProjectService: IUserProjectService,
    @Inject(MAIL_SERVICE)
    private readonly mailService: IMailService,
    @Inject(PINO_LOGGER_SERVICE)
    private readonly logger: PinoLoggerService,
  ) {
    logger.setContext('InviteService');
  }

  async createInvite(expiresDate: Date): Promise<IInvite> {
    return await this.inviteRepository.createInvite(expiresDate);
  }

  async checkUUID(inviteUuid: string): Promise<IInvitesUsersRolesProjectsCoherence> {
    if (!isValidUUID(inviteUuid)) {
      this.logger.warn('Invalid UUID format');
      throw new BadRequestException('Invalid UUID format');
    }
    const invite = await this.inviteRepository.findByInviteUuid(inviteUuid);
    if (!invite) {
      this.logger.warn(`The invitation with UUID=${inviteUuid} doesn't exist`);
      throw new NotFoundException(`The invitation with UUID=${inviteUuid} doesn't exist`);
    }
    const coherence = await this.inviteCoherenceService.findCoherenceByInviteId(invite.id);
    if (!coherence) {
      this.logger.warn('The invitation has been canceled');
      throw new GoneException('The invitation has been canceled');
    }
    return coherence;
  }

  async deleteInviteByUuid(inviteUuid: string): Promise<void> {
    const result = await this.inviteRepository.deleteInviteByUuid(inviteUuid);
    if (!result) {
      this.logger.warn(`The invitation with UUID=${inviteUuid} doesn't exist`);
      throw new NotFoundException(`The invitation with UUID=${inviteUuid} doesn't exist`);
    }
  }

  async subscribeUser(coherence: IInvitesUsersRolesProjectsCoherence): Promise<void> {
    coherence.user.role = coherence.role;
    coherence.user.active = true;
    delete coherence.user.email;
    await this.userService.update(coherence.user.id, coherence.user);
    await this.userProjectService.createProject({
      projectId: coherence.project,
      userId: coherence.user,
    });
  }

  async inviteResolver(inviteUuid: string): Promise<void> {
    const coherence = await this.checkUUID(inviteUuid);
    if (!coherence.userId) {
      this.logger.warn('The registration is required to continue');
      throw new UnprocessableEntityException('The registration is required to continue');
    }
    await this.subscribeUser(coherence);
    await this.deleteInviteByUuid(inviteUuid);
  }

  async createAndSendInvite(createInvite: IInviteCreate, httpProtocol: string): Promise<void> {
    const expiresAt = generateExpiresDate();
    const invite = await this.createInvite(expiresAt);
    const inviteLink = generateInviteLink(invite.invite, httpProtocol);
    const user = await this.userService.findOneByEmail(createInvite.email);
    await this.inviteCoherenceService.createInviteCoherence({
      ...createInvite,
      inviteId: invite.id,
      userId: user?.id,
      expiresAt,
    });
    await this.mailService.notifyUserOnMail(createInvite.email, {
      text: inviteLink,
      subject: 'Invitation Service Link',
    });
  }

  async createUserAndSubscribe(inviteUuid: string, createUser: ICreateUser): Promise<void> {
    const coherence = await this.checkUUID(inviteUuid);
    createUser.active = true;
    coherence.user = await this.userService.create(createUser, coherence.role.roleName);
    await this.subscribeUser(coherence);
    await this.deleteInviteByUuid(inviteUuid);
  }

  @Cron(CronExpression.EVERY_DAY_AT_MIDNIGHT)
  async deleteExpiredInvites(): Promise<void> {
    const expiredInvites = await this.inviteRepository.findExpiredInvites();
    const expiredIds = expiredInvites.map((invite: IInvite) => invite?.id);
    if (expiredIds.length) await this.inviteRepository.deleteInvites(expiredIds);
  }
}
