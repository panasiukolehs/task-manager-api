import { Injectable } from '@nestjs/common';

import { InvitesUsersRolesProjectsCoherenceEntity } from '../../entities/invites-users-roles-projects-coherence.entity';
import { RepositoryDAO } from '../../../shared/dao/repositories/typeorm-repository.dao';
import { IRepository } from '../../../shared/dao/types/base-repository.interface';
import { IInvitesUsersRolesProjectsCoherence } from '../../interfaces/invites-users-roles-projects-coherence.interface';
import { IInviteCoherenceCreate } from '../../interfaces/invite-coherence-create.interface';

@Injectable()
export class TypeormInvitesUsersRolesProjectsCoherenceRepositoryDao
  extends RepositoryDAO<InvitesUsersRolesProjectsCoherenceEntity>
  implements IRepository<InvitesUsersRolesProjectsCoherenceEntity>
{
  async find(): Promise<IInvitesUsersRolesProjectsCoherence[]> {
    const repository = await this._getRepository(InvitesUsersRolesProjectsCoherenceEntity);
    return await repository.find();
  }

  async findById(id: number): Promise<IInvitesUsersRolesProjectsCoherence> {
    const repository = await this._getRepository(InvitesUsersRolesProjectsCoherenceEntity);
    return await repository.findOne(id);
  }

  async findCoherenceByInviteId(inviteId: number): Promise<IInvitesUsersRolesProjectsCoherence> {
    const repository = await this._getRepository(InvitesUsersRolesProjectsCoherenceEntity);
    return await repository.findOne({ inviteId });
  }

  async createInviteCoherence(
    createCoherence: IInviteCoherenceCreate,
  ): Promise<IInvitesUsersRolesProjectsCoherence> {
    const repository = await this._getRepository(InvitesUsersRolesProjectsCoherenceEntity);
    return await repository.save(createCoherence);
  }
}
