import { Injectable } from '@nestjs/common';

import { InviteEntity } from '../../entities/invite.entity';
import { RepositoryDAO } from '../../../shared/dao/repositories/typeorm-repository.dao';
import { IRepository } from '../../../shared/dao/types/base-repository.interface';
import { IInvite } from '../../interfaces/invite.interface';

@Injectable()
export class TypeormInviteRepositoryDAO
  extends RepositoryDAO<InviteEntity>
  implements IRepository<InviteEntity>
{
  async find(): Promise<IInvite[]> {
    const repository = await this._getRepository(InviteEntity);
    return await repository.find();
  }

  async findById(id: number): Promise<IInvite> {
    const repository = await this._getRepository(InviteEntity);
    return await repository.findOne(id);
  }

  async findByInviteUuid(inviteUuid: string): Promise<IInvite> {
    const repository = await this._getRepository(InviteEntity);
    return await repository.findOne({ invite: inviteUuid });
  }

  async createInvite(expiresAt: Date): Promise<IInvite> {
    const repository = await this._getRepository(InviteEntity);
    return await repository.save({ expiresAt });
  }

  async deleteInviteByUuid(inviteUuid: string): Promise<boolean> {
    const repository = await this._getRepository(InviteEntity);
    return !!(await repository.delete({ invite: inviteUuid })).affected;
  }

  async deleteInvites(inviteIds: number[]): Promise<boolean> {
    const repository = await this._getRepository(InviteEntity);
    return !!(await repository.delete(inviteIds)).affected;
  }

  async findExpiredInvites(): Promise<IInvite[]> {
    const repository = await this._getRepository(InviteEntity);
    return await repository
      .createQueryBuilder('invites')
      .where('invites.expiresAt <= :currentDate', { currentDate: new Date() })
      .getMany();
  }
}
