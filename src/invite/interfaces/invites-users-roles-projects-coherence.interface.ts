import { IInvite } from './invite.interface';
import { IProject } from '../../project/interfaces/project-response.interface';
import { IRole } from '../../role/interfaces/IRole';
import { IUser } from '../../user/interfaces/IUser';

export interface IInvitesUsersRolesProjectsCoherence {
  id: number;
  userId: number;
  inviteId: number;
  projectId: number;
  roleId: number;
  user?: IUser;
  invite?: IInvite;
  project?: IProject;
  role?: IRole;
  createdAt?: Date;
  updatedAt?: Date;
  expiresAt: Date;
}
