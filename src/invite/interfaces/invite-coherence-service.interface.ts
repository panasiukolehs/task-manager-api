import { IInvitesUsersRolesProjectsCoherence } from './invites-users-roles-projects-coherence.interface';
import { IInviteCoherenceCreate } from './invite-coherence-create.interface';

export interface IInviteCoherenceService {
  findCoherenceByInviteId(inviteId: number): Promise<IInvitesUsersRolesProjectsCoherence>;
  createInviteCoherence(
    createCoherence: IInviteCoherenceCreate,
  ): Promise<IInvitesUsersRolesProjectsCoherence>;
}
