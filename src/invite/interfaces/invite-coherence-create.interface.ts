export interface IInviteCoherenceCreate {
  userId?: number;
  inviteId: number;
  projectId: number;
  roleId: number;
  expiresAt: Date;
}
