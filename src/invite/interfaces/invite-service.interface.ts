import { IInviteCreate } from './invite-create.interface';
import { IInvite } from './invite.interface';
import { IInvitesUsersRolesProjectsCoherence } from './invites-users-roles-projects-coherence.interface';
import { ICreateUser } from '../../user/interfaces/ICreateUser';

export interface IInviteService {
  createAndSendInvite(createInvite: IInviteCreate, httpProtocol: string): Promise<void>;
  createInvite(expiresDate: Date): Promise<IInvite>;
  inviteResolver(inviteUuid: string): Promise<void>;
  deleteInviteByUuid(inviteUuid: string): Promise<void>;
  subscribeUser(coherence: IInvitesUsersRolesProjectsCoherence): Promise<void>;
  createUserAndSubscribe(inviteUuid: string, createUser: ICreateUser): Promise<void>;
  deleteExpiredInvites(): Promise<void>;
  checkUUID(inviteUuid: string): Promise<IInvitesUsersRolesProjectsCoherence>;
}
