export interface IInvite {
  id: number;
  invite: string;
  createdAt?: Date;
  updatedAt?: Date;
  expiresAt: Date;
}
