export interface IInviteCreate {
  email: string;
  projectId: number;
  roleId: number;
}
