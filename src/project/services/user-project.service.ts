import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { RepositoryTypes } from '../../shared/dao/types/repository-types.enum';
import { userProjectJuncitonRepositoryType } from '../../shared/dao/ConnectionDAO';
import { UserProjectJunction } from '../entities/userProjectJunction.entity';
import { DeleteResult } from 'typeorm';
import { IUserProjectJunction } from '../interfaces/IProjectUserJunction';
import { PINO_LOGGER_SERVICE } from '../../constants/serviceConstants';
import { PinoLoggerService } from '../../logger/services/pino-logger.service';

@Injectable()
export class UserProjectService {
  constructor(
    @Inject(RepositoryTypes.PROJECT_USER_JUNCTION_REPOSITORY)
    private userProjectJunctionRepository: userProjectJuncitonRepositoryType,
    @Inject(PINO_LOGGER_SERVICE)
    private readonly logger: PinoLoggerService,
  ) {
    logger.setContext('UserProjectService');
  }

  async checkIfUserBelongsToProject(userId: number, projectId: number): Promise<boolean> {
    const result = await this.userProjectJunctionRepository.findById(userId, projectId);
    if (!result) {
      const errorMessage = `User doesn't belong to project.`;
      this.logger.warn(errorMessage);
      throw new HttpException(
        `User doesn't belong to a project ${projectId}`,
        HttpStatus.NOT_FOUND,
      );
    }

    return !!result;
  }

  async find(userId: number): Promise<UserProjectJunction[]> {
    return await this.userProjectJunctionRepository.find(userId);
  }

  async findById(userId: number, projectId: number): Promise<UserProjectJunction> {
    return await this.userProjectJunctionRepository.findById(userId, projectId);
  }

  async createProject(userProjectObj: IUserProjectJunction): Promise<UserProjectJunction> {
    return await this.userProjectJunctionRepository.createProject(userProjectObj);
  }

  async deleteProject(projectId: number): Promise<DeleteResult> {
    return await this.userProjectJunctionRepository.deleteProject(projectId);
  }
}
