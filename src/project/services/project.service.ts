import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { DeleteResult } from 'typeorm';
import { ICreateProject } from '../interfaces/project-create.interface';
import { IProjectUpdate } from '../interfaces/project-update.interface';
import { IProject } from '../interfaces/project-response.interface';
import { IUser } from '../../user/interfaces/IUser';
import { RepositoryTypes } from '../../shared/dao/types/repository-types.enum';
import { projectRepositoryType } from '../../shared/dao/ConnectionDAO';
import {
  BACKLOG_SERVICE,
  PINO_LOGGER_SERVICE,
  USER_PROJECT_SERVICE,
} from '../../constants/serviceConstants';
import { IBacklogService } from '../../backlog/interfaces/IBacklogService';
import { IBacklog } from 'src/backlog/interfaces/IBacklog';
import { IUserProjectService } from '../interfaces/IUserProjectService';
import { PinoLoggerService } from '../../logger/services/pino-logger.service';

@Injectable()
export class ProjectService {
  constructor(
    @Inject(RepositoryTypes.PROJECT_REPOSITORY)
    private projectRepository: projectRepositoryType,
    @Inject(BACKLOG_SERVICE)
    private backlogService: IBacklogService,
    @Inject(USER_PROJECT_SERVICE)
    private userProjectJunctionService: IUserProjectService,
    @Inject(PINO_LOGGER_SERVICE)
    private readonly logger: PinoLoggerService,
  ) {
    logger.setContext('ProjectService');
  }

  async getCurrentProject(userId: number, projectId: number): Promise<IProject> {
    await this.userProjectJunctionService.checkIfUserBelongsToProject(userId, projectId);
    return await this.projectRepository.findById(projectId);
  }

  async getProjectById(projectId: number): Promise<IProject> {
    return await this.projectRepository.findById(projectId);
  }

  async getProjectBacklog(userId: number, projectId: number): Promise<IBacklog> {
    await this.userProjectJunctionService.checkIfUserBelongsToProject(userId, projectId);
    return await this.backlogService.findByProjectId(projectId);
  }

  async getAllProjects(userId: number): Promise<IProject[]> {
    const found = await this.userProjectJunctionService.find(userId);
    if (!found) {
      this.logger.warn(`User don't have access to any projects.`);
      throw new NotFoundException(`You don't have access to any projects.`);
    }
    const ids = found.map((obj) => obj.projectId);
    return this.projectRepository.find(ids);
  }

  async createProject(user: IUser, newProject: ICreateProject): Promise<IProject> {
    const project = await this.projectRepository.createProject(newProject);
    await this.backlogService.createBacklog(project.id);
    await this.userProjectJunctionService.createProject({ projectId: project, userId: user });
    return project;
  }

  async updateProject(
    userId: number,
    projectId: number,
    updateProject: IProjectUpdate,
  ): Promise<IProject> {
    await this.userProjectJunctionService.checkIfUserBelongsToProject(userId, projectId);
    return this.projectRepository.updateProject(projectId, updateProject);
  }

  async deleteProject(userId: number, projectId: number): Promise<DeleteResult> {
    await this.userProjectJunctionService.checkIfUserBelongsToProject(userId, projectId);
    const result = await this.projectRepository.deleteProject(projectId);
    if (result.affected === 0) {
      this.logger.warn(`No project with such id`);
      throw new NotFoundException(`Project with ${projectId} not found`);
    }
    return result;
  }
}
