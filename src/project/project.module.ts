import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ProjectEntity } from './entities/project.entity';
import { ProjectController } from './controllers/project.controller';
import {
  diBacklogService,
  diLoggerService,
  diProjectService,
  diTaskService,
  diUserProjectService,
} from '../DIConfig/serviceConfig';
import {
  backlogRepositoryDAO,
  projectRepositoryDAO,
  taskRepositoryDAO,
  userProjectJucntionRepositoryDAO,
} from '../shared/dao/ConnectionDAO';
import { LoggerModule } from '../logger/logger.module';

@Module({
  imports: [TypeOrmModule.forFeature([ProjectEntity]), LoggerModule],
  providers: [
    diProjectService,
    projectRepositoryDAO,
    diBacklogService,
    backlogRepositoryDAO,
    diTaskService,
    taskRepositoryDAO,
    userProjectJucntionRepositoryDAO,
    diUserProjectService,
    diLoggerService,
  ],
  controllers: [ProjectController],
  exports: [diProjectService, projectRepositoryDAO],
})
export class ProjectModule {}
