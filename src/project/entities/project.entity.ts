import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('projects')
export class ProjectEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ default: '' })
  description?: string;

  @Column({ default: true })
  status?: boolean;

  @Column({ name: 'created_at', select: false })
  createdAt?: Date;

  @Column({ name: 'updated_at', select: false })
  updatedAt?: Date;
}
