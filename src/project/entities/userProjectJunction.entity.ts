import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../../user/entities/user.entity';
import { IUser } from '../../user/interfaces/IUser';
import { IUserProjectJunction } from '../interfaces/IProjectUserJunction';
import { IProject } from '../interfaces/project-response.interface';
import { ProjectEntity } from './project.entity';

@Entity('user_project_junction')
export class UserProjectJunction implements IUserProjectJunction {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'project_id' })
  @ManyToOne(() => ProjectEntity, { onDelete: 'CASCADE' })
  @JoinColumn([{ name: 'project_id', referencedColumnName: 'id' }])
  projectId: IProject;

  @Column({ name: 'user_id' })
  @ManyToOne(() => User, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  userId: IUser;
}
