import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ProjectUpdateDto } from '../dto/project-update.dto';
import { ProjectCreateDto } from '../dto/project-create.dto';
import { IProject } from '../interfaces/project-response.interface';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { PROJECT_SERVICE, TASK_SERVICE } from '../../constants/serviceConstants';
import { IProjectsService } from '../interfaces/IProjectService';
import { DeleteResult } from 'typeorm';
import { IUserRequest } from '../interfaces/IUserRequest';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { ProjectDto } from '../../shared/swagger/dto/project.dto';
import { SingleErrorDto } from '../../shared/swagger/dto/single-error.dto';
import { Roles } from 'src/role-based-auth/decorators/role.decorator';
import { PROJECT_MANAGER } from 'src/constants/roleConstants';
import { RolesGuard } from 'src/role-based-auth/roles.guard';
import { TaskDto } from 'src/shared/swagger/dto/task.dto';
import { ITask } from 'src/task/interfaces/ITask';
import { IUserRequestModel } from 'src/auth/interfaces/IUserRequestModel';
import { ITaskService } from 'src/task/interfaces/ITaskService';
import { TaskType } from 'src/task/types/task-type';

@ApiTags('projects')
@Controller('project')
@UseGuards(JwtAuthGuard)
export class ProjectController {
  constructor(
    @Inject(PROJECT_SERVICE)
    private projectService: IProjectsService,
    @Inject(TASK_SERVICE)
    private taskService: ITaskService,
  ) {}

  @ApiOkResponse({
    description: 'Get project by ID',
    type: ProjectDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Get(':id')
  async getCurrentProject(
    @Request() req: IUserRequest,
    @Param('id', ParseIntPipe) id: number,
  ): Promise<IProject> {
    return await this.projectService.getCurrentProject(req.user.id, id);
  }

  @ApiOkResponse({
    description: 'Get all projects',
    type: [ProjectDto],
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Get()
  async getAllProject(@Request() req: IUserRequest): Promise<IProject[]> {
    return await this.projectService.getAllProjects(req.user.id);
  }

  @ApiCreatedResponse({
    description: 'Successful creation',
    type: ProjectDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Roles(PROJECT_MANAGER)
  @UseGuards(RolesGuard)
  @Post()
  async createProject(
    @Request() req: IUserRequest,
    @Body() newProject: ProjectCreateDto,
  ): Promise<IProject> {
    return this.projectService.createProject(req.user.id, newProject);
  }

  @ApiOkResponse({
    description: 'Successful update',
    type: String,
  })
  @ApiNotFoundResponse({
    description: 'Project ID is incorrect',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Roles(PROJECT_MANAGER)
  @UseGuards(RolesGuard)
  @Put(':id')
  async updateProject(
    @Request() req: IUserRequest,
    @Param('id', ParseIntPipe) id: number,
    @Body() updateProject: ProjectUpdateDto,
  ): Promise<IProject> {
    return await this.projectService.updateProject(req.user.id, id, updateProject);
  }

  @ApiOkResponse({
    description: 'Successful deleting',
    type: String,
  })
  @ApiNotFoundResponse({
    description: "Project with specified ID doesn't exist",
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Roles(PROJECT_MANAGER)
  @UseGuards(RolesGuard)
  @Delete(':id')
  async deleteProject(
    @Request() req: IUserRequest,
    @Param('id', ParseIntPipe) id: number,
  ): Promise<DeleteResult> {
    return this.projectService.deleteProject(req.user.id, id);
  }

  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Find all tasks for specific project',
    type: TaskDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Get('/:projectId/tasks')
  async findAllTasksForBacklog(
    @Param('projectId', ParseIntPipe) projectId: number,
    @Request() req: IUserRequestModel,
    @Query('type') type: TaskType,
  ): Promise<ITask[]> {
    const backlog = await this.projectService.getProjectBacklog(req.user.id, projectId);
    if (!type) return await this.taskService.findAllTasksForBacklog(backlog.id);
    return await this.taskService.findSpecificTypeTasksForBacklog(backlog.id, type);
  }
}
