import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString, MaxLength } from 'class-validator';
import { IProjectUpdate } from '../interfaces/project-update.interface';

export class ProjectUpdateDto implements IProjectUpdate {
  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  name: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  @MaxLength(255)
  description: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsBoolean()
  status: boolean;
}
