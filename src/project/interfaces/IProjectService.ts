import { IBacklog } from 'src/backlog/interfaces/IBacklog';
import { DeleteResult } from 'typeorm';
import { ICreateProject } from './project-create.interface';
import { IProject } from './project-response.interface';
import { IProjectUpdate } from './project-update.interface';

export interface IProjectsService {
  getCurrentProject(userId: number, projectId: number): Promise<IProject>;

  getAllProjects(userId: number): Promise<IProject[]>;

  createProject(userId: number, newProject: ICreateProject): Promise<IProject>;

  updateProject(
    userId: number,
    projectId: number,
    updateProject: IProjectUpdate,
  ): Promise<IProject>;

  deleteProject(userId: number, projectId: number): Promise<DeleteResult>;

  getProjectBacklog(userId: number, projectId: number): Promise<IBacklog>;

  getProjectById(projectId: number): Promise<IProject>;
}
