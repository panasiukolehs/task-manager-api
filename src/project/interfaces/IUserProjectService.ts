import { DeleteResult } from 'typeorm';
import { UserProjectJunction } from '../entities/userProjectJunction.entity';
import { IUserProjectJunction } from './IProjectUserJunction';

export interface IUserProjectService {
  checkIfUserBelongsToProject(userId: number, projectId: number): Promise<boolean>;

  find(userId: number): Promise<UserProjectJunction[]>;

  findById(userId: number, projectId: number): Promise<UserProjectJunction>;

  createProject(userProjectObj: IUserProjectJunction): Promise<UserProjectJunction>;

  deleteProject(projectId: number): Promise<DeleteResult>;
}
