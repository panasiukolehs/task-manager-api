export interface ICreateProject {
  name: string;
  description?: string;
  status?: boolean;
}
