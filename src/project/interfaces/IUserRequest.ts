import { Request } from 'express';
import { IUser } from '../../user/interfaces/IUser';

export interface IUserRequest extends Request {
  user: IUser;
}
