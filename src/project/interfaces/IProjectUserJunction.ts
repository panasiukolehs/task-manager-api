import { IUser } from '../../user/interfaces/IUser';
import { IProject } from './project-response.interface';

export interface IUserProjectJunction {
  projectId: IProject;
  userId: IUser;
}
