export interface IProjectUpdate {
  name?: string;
  description?: string;
  status?: boolean;
}
