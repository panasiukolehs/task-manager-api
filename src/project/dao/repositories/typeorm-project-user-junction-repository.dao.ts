import { Injectable } from '@nestjs/common';
import { DeleteResult } from 'typeorm';
import { RepositoryDAO } from '../../../shared/dao/repositories/typeorm-repository.dao';
import { UserProjectJunction } from '../../entities/userProjectJunction.entity';
import { IUserProjectJunction } from '../../interfaces/IProjectUserJunction';
@Injectable()
export class TypeormUserProjectJunctionRepositoryDAO extends RepositoryDAO<UserProjectJunction> {
  constructor() {
    super();
  }

  async find(userId: number): Promise<UserProjectJunction[]> {
    const userProjectRepository = await this._getRepository(UserProjectJunction);
    const found = await userProjectRepository
      .createQueryBuilder()
      .where('user_id = :userId', { userId })
      .getMany();
    return found;
  }

  async findById(userId: number, projectId: number): Promise<UserProjectJunction> {
    const userProjectRepository = await this._getRepository(UserProjectJunction);
    const found = await userProjectRepository
      .createQueryBuilder()
      .where('project_id = :projectId AND user_id = :userId', { projectId, userId })
      .getOne();
    return found;
  }

  async createProject(userProjectObj: IUserProjectJunction): Promise<UserProjectJunction> {
    const userProjectRepository = await this._getRepository(UserProjectJunction);
    return await userProjectRepository.save(userProjectObj);
  }

  async deleteProject(projectId: number): Promise<DeleteResult> {
    const userProjectRepository = await this._getRepository(UserProjectJunction);
    const result = userProjectRepository
      .createQueryBuilder()
      .delete()
      .where('project_id = :projectId', { projectId })
      .execute();
    return result;
  }
}
