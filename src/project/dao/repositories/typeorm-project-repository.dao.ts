import { Injectable } from '@nestjs/common';
import { DeleteResult } from 'typeorm';
import { RepositoryDAO } from '../../../shared/dao/repositories/typeorm-repository.dao';
import { ProjectEntity } from '../../entities/project.entity';
import { ICreateProject } from '../../interfaces/project-create.interface';
import { IProject } from '../../interfaces/project-response.interface';
import { IProjectUpdate } from '../../interfaces/project-update.interface';

@Injectable()
export class TypeormProjectRepositoryDAO extends RepositoryDAO<ProjectEntity> {
  constructor() {
    super();
  }

  async find(ids: IProject[]): Promise<IProject[]> {
    const projectRepository = await this._getRepository(ProjectEntity);
    const projects = await projectRepository
      .createQueryBuilder()
      .where('id IN (:...ids)', { ids: ids })
      .getMany();
    return projects;
  }

  async findById(id: number): Promise<IProject> {
    const projectRepository = await this._getRepository(ProjectEntity);
    return await projectRepository.findOne(id);
  }

  async createProject(newProject: ICreateProject): Promise<IProject> {
    const projectRepository = await this._getRepository(ProjectEntity);
    return await projectRepository.save(newProject);
  }

  async updateProject(projectId: number, updateProject: IProjectUpdate): Promise<IProject> {
    const projectRepository = await this._getRepository(ProjectEntity);
    const project = await projectRepository.findOne(projectId);
    Object.assign(project, updateProject);
    return await projectRepository.save(project);
  }

  async deleteProject(projectId: number): Promise<DeleteResult> {
    const projectRepository = await this._getRepository(ProjectEntity);
    return await projectRepository.delete(projectId);
  }
}
