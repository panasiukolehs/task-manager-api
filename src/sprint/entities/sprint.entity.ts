import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { ProjectEntity } from '../../project/entities/project.entity';

@Entity('sprints')
export class SprintEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  name: string;

  @Column({ default: true })
  status: boolean;

  @Column({ name: 'date_of_period' })
  dateOfPeriod: Date;

  @ManyToOne(() => ProjectEntity)
  @JoinColumn({ name: 'projectId', referencedColumnName: 'id' })
  project?: ProjectEntity;

  @Column({ type: 'int', nullable: false })
  projectId: number;

  @Column({ name: 'created_at' })
  createdAt?: Date;

  @Column({ name: 'updated_at' })
  updatedAt?: Date;
}
