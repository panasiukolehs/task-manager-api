import { HttpException, HttpStatus, Inject, Injectable, NotFoundException } from '@nestjs/common';

import { RepositoryTypes } from '../../shared/dao/types/repository-types.enum';
import { sprintRepositoryType } from '../../shared/dao/ConnectionDAO';
import { ISprint } from '../interfaces/sprint.interface';
import { ISprintCreate } from '../interfaces/sprint-create.interface';
import { ISprintUpdate } from '../interfaces/sprint-update.interface';
import { PINO_LOGGER_SERVICE } from '../../constants/serviceConstants';
import { PinoLoggerService } from '../../logger/services/pino-logger.service';

@Injectable()
export class SprintService {
  constructor(
    @Inject(RepositoryTypes.SPRINT_REPOSITORY)
    private readonly sprintRepository: sprintRepositoryType,
    @Inject(PINO_LOGGER_SERVICE)
    private readonly logger: PinoLoggerService,
  ) {
    logger.setContext('SprintService');
  }

  async getAll(projectId: number): Promise<ISprint[]> {
    return await this.sprintRepository.findAllSprintsByProjectId(projectId);
  }

  async getSprint(currentSprintId: number): Promise<ISprint> {
    const sprint = await this.sprintRepository.findById(currentSprintId);
    if (!sprint) {
      this.logger.warn("Sprint with specified ID doesn't exist");
      throw new HttpException("Sprint with specified ID doesn't exist", HttpStatus.NOT_FOUND);
    }
    return sprint;
  }

  async updateSprint(currentSprintId: number, sprintUpdates: ISprintUpdate): Promise<string> {
    const result = await this.sprintRepository.updateSprint(currentSprintId, sprintUpdates);
    if (result) {
      return 'Sprint has been updated successfully';
    }
    this.logger.warn("Nothing to update. Sprint with specified ID doesn't exist.");
    throw new NotFoundException("Nothing to update. Sprint with specified ID doesn't exist.");
  }

  async createSprint(sprint: ISprintCreate): Promise<ISprint> {
    try {
      return await this.sprintRepository.createSprint(sprint);
    } catch (_) {
      this.logger.warn("Incorrect project ID. Project with specified ID doesn't exist.");
      throw new NotFoundException("Incorrect project ID. Project with specified ID doesn't exist.");
    }
  }

  async deleteSprint(currentSprintId: number): Promise<string> {
    const result = await this.sprintRepository.deleteSprint(currentSprintId);
    if (result) {
      return 'Sprint has been deleted successfully';
    }
    this.logger.warn("Nothing to delete. Sprint with specified ID doesn't exist.");
    throw new NotFoundException("Nothing to delete. Sprint with specified ID doesn't exist.");
  }
}
