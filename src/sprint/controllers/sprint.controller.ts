import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { PROJECT_MANAGER } from 'src/constants/roleConstants';
import { Roles } from 'src/role-based-auth/decorators/role.decorator';
import { RolesGuard } from 'src/role-based-auth/roles.guard';
import { TaskDto } from 'src/shared/swagger/dto/task.dto';
import { ITask } from 'src/task/interfaces/ITask';
import { ITaskService } from 'src/task/interfaces/ITaskService';
import { TaskType } from 'src/task/types/task-type';

import { SPRINT_SERVICE, TASK_SERVICE } from '../../constants/serviceConstants';
import { SingleErrorDto } from '../../shared/swagger/dto/single-error.dto';
import { SprintDto } from '../../shared/swagger/dto/sprint.dto';
import { SprintCreateDto } from '../dto/sprint-create.dto';
import { SprintUpdateDto } from '../dto/sprint-update.dto';
import { ISprintService } from '../interfaces/sprint-service.interface';
import { ISprint } from '../interfaces/sprint.interface';
@UseGuards(JwtAuthGuard)
@ApiTags('sprints')
@Controller()
export class SprintController {
  constructor(
    @Inject(SPRINT_SERVICE)
    private readonly sprintService: ISprintService,
    @Inject(TASK_SERVICE)
    private readonly taskService: ITaskService,
  ) {}

  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get all sprints',
    type: [SprintDto],
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Get('sprints/:projectId')
  async getAll(@Param('projectId') projectId: number): Promise<ISprint[]> {
    return await this.sprintService.getAll(projectId);
  }

  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get sprint by ID',
    type: SprintDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Get('sprint/:sprintId')
  async getSprint(@Param('sprintId') currentSprintId: number): Promise<ISprint | object> {
    return await this.sprintService.getSprint(currentSprintId);
  }

  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Find all tasks for specific sprint',
    type: TaskDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Get('sprint/:sprintId/tasks')
  async findAllTasksForSprint(
    @Param('sprintId', ParseIntPipe) sprintId: number,
    @Query('type') type: TaskType,
  ): Promise<ITask[]> {
    if (!type) return await this.taskService.findAllTasksForSprint(sprintId);
    return await this.taskService.findSpecificTypeTasksForSprint(sprintId, type);
  }

  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Successfully updated!',
    type: TaskDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Roles(PROJECT_MANAGER)
  @UseGuards(RolesGuard)
  @Post('sprint/:sprintId/task/:taskId')
  async updateTaskSprint(
    @Param('taskId', ParseIntPipe) taskId: number,
    @Param('sprintId', ParseIntPipe) sprintId: number,
  ): Promise<ITask> {
    await this.sprintService.getSprint(sprintId);
    return await this.taskService.assingTaskToSprint(taskId, sprintId);
  }

  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Successful update',
    type: String,
  })
  @ApiNotFoundResponse({
    description: 'Sprint ID is incorrect',
    type: SingleErrorDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Put('sprint/:sprintId')
  async updateSprint(
    @Param('sprintId') id: number,
    @Body() sprintUpdates: SprintUpdateDto,
  ): Promise<string> {
    return await this.sprintService.updateSprint(id, sprintUpdates);
  }

  @ApiBearerAuth()
  @ApiCreatedResponse({
    description: 'Successful creation',
    type: SprintDto,
  })
  @ApiNotFoundResponse({
    description: 'Project ID is incorrect',
    type: SingleErrorDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Post('sprint')
  async createSprint(@Body() sprint: SprintCreateDto): Promise<ISprint> {
    return await this.sprintService.createSprint(sprint);
  }

  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Successful deleting',
    type: String,
  })
  @ApiNotFoundResponse({
    description: "Sprint with specified ID doesn't exist",
    type: SingleErrorDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Delete('sprint/:sprintId')
  async deleteSprint(@Param('sprintId') currentSprintId: number): Promise<string> {
    return this.sprintService.deleteSprint(currentSprintId);
  }
}
