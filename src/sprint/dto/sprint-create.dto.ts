import { IsDateString, IsInt, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

import { ISprintCreate } from '../interfaces/sprint-create.interface';

export class SprintCreateDto implements ISprintCreate {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsDateString()
  dateOfPeriod: Date;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  @IsInt()
  projectId: number;
}
