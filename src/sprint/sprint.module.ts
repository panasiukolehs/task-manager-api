import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { SprintController } from './controllers/sprint.controller';
import { diLoggerService, diSprintService, diTaskService } from '../DIConfig/serviceConfig';
import { sprintRepositoryDAO, taskRepositoryDAO } from '../shared/dao/ConnectionDAO';
import { SprintEntity } from './entities/sprint.entity';

@Module({
  imports: [TypeOrmModule.forFeature([SprintEntity])],
  controllers: [SprintController],
  providers: [
    diSprintService,
    sprintRepositoryDAO,
    diTaskService,
    taskRepositoryDAO,
    diLoggerService,
  ],
  exports: [diSprintService, sprintRepositoryDAO],
})
export class SprintModule {}
