export interface ISprintUpdate {
  name?: string;
  status?: boolean;
  dateOfPeriod?: Date;
}
