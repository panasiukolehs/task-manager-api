export interface ISprintCreate {
  name: string;
  dateOfPeriod: Date;
  projectId: number;
}
