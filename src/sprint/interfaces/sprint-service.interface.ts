import { ISprint } from './sprint.interface';
import { ISprintCreate } from './sprint-create.interface';
import { ISprintUpdate } from './sprint-update.interface';

export interface ISprintService {
  getAll(projectId: number): Promise<ISprint[]>;
  getSprint(currentSprintId: number): Promise<ISprint>;
  createSprint(sprint: ISprintCreate): Promise<ISprint>;
  updateSprint(currentSprintId: number, sprintUpdates: ISprintUpdate): Promise<string>;
  deleteSprint(currentSprintId: number): Promise<string>;
}
