import { IProject } from '../../project/interfaces/project-response.interface';

export interface ISprint {
  id: number;
  name: string;
  status: boolean;
  dateOfPeriod: Date;
  project?: IProject;
  projectId: number;
  createdAt?: Date;
  updatedAt?: Date;
}
