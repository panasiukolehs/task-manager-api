import { Injectable } from '@nestjs/common';

import { RepositoryDAO } from '../../../shared/dao/repositories/typeorm-repository.dao';
import { SprintEntity } from '../../entities/sprint.entity';
import { IRepository } from '../../../shared/dao/types/base-repository.interface';
import { ISprint } from '../../interfaces/sprint.interface';
import { ISprintCreate } from '../../interfaces/sprint-create.interface';
import { ISprintUpdate } from '../../interfaces/sprint-update.interface';

@Injectable()
export class TypeormSprintRepositoryDAO
  extends RepositoryDAO<SprintEntity>
  implements IRepository<SprintEntity>
{
  async find(): Promise<ISprint[]> {
    const repository = await this._getRepository(SprintEntity);
    return await repository.find();
  }

  async findAllSprintsByProjectId(projectId: number): Promise<ISprint[]> {
    const repository = await this._getRepository(SprintEntity);
    return await repository.find({ where: { projectId } });
  }

  async findAllSprintsByProjectIdWithProjectRelations(projectId: number): Promise<ISprint[]> {
    const repository = await this._getRepository(SprintEntity);
    return await repository.find({ relations: ['project'], where: { projectId } });
  }

  async findById(id: number): Promise<ISprint> {
    const repository = await this._getRepository(SprintEntity);
    return await repository.findOne(id);
  }

  async createSprint(sprint: ISprintCreate): Promise<ISprint> {
    const repository = await this._getRepository(SprintEntity);
    return await repository.save(sprint);
  }

  async updateSprint(currentSprintId: number, sprintUpdates: ISprintUpdate): Promise<boolean> {
    const repository = await this._getRepository(SprintEntity);
    return !!(await repository.update(currentSprintId, sprintUpdates)).affected;
  }

  async deleteSprint(currentSprintId: number): Promise<boolean> {
    const repository = await this._getRepository(SprintEntity);
    return !!(await repository.delete(currentSprintId)).affected;
  }
}
