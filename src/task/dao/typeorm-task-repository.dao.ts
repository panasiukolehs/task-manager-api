import { DeleteResult } from 'typeorm';
import { Injectable } from '@nestjs/common';

import { IRepository } from 'src/shared/dao/types/base-repository.interface';
import { RepositoryDAO } from 'src/shared/dao/repositories/typeorm-repository.dao';
import { TaskEntity } from '../entities/task.entity';
import { ITask } from '../interfaces/ITask';
import { TaskType } from '../types/task-type';

@Injectable()
export class TypeormTaskRepositoryDAO
  extends RepositoryDAO<TaskEntity>
  implements IRepository<TaskEntity>
{
  constructor() {
    super();
  }

  async find(): Promise<TaskEntity[]> {
    const repository = await this._getRepository(TaskEntity);
    return await repository.find();
  }

  async findWithBacklogId(backlogId: number): Promise<TaskEntity[]> {
    const repository = await this._getRepository(TaskEntity);
    return await repository.find({ backlogId });
  }

  async findWithSprintId(sprintId: number): Promise<TaskEntity[]> {
    const repository = await this._getRepository(TaskEntity);
    return await repository.find({ sprintId });
  }

  async findWithTypeForSprint(type: TaskType, sprintId: number): Promise<TaskEntity[]> {
    const repository = await this._getRepository(TaskEntity);
    return await repository
      .createQueryBuilder()
      .where('sprint_id = :sprintId', { sprintId })
      .andWhere('type = :type', { type })
      .getMany();
  }

  async findWithTypeForBacklog(type: TaskType, backlogId: number): Promise<TaskEntity[]> {
    const repository = await this._getRepository(TaskEntity);
    return await repository
      .createQueryBuilder()
      .where('backlog_id = :backlogId', { backlogId })
      .andWhere('type = :type', { type })
      .getMany();
  }

  async findById(id: number): Promise<TaskEntity> {
    const repository = await this._getRepository(TaskEntity);
    return await repository.findOne(id);
  }

  async createTask(newTask: ITask): Promise<TaskEntity> {
    const repository = await this._getRepository(TaskEntity);
    return await repository.save(newTask);
  }

  async updateTask(id: number, taskUpdates: ITask): Promise<boolean> {
    const repository = await this._getRepository(TaskEntity);
    const result = await repository
      .createQueryBuilder()
      .update()
      .set({ ...taskUpdates })
      .where('id = :id', { id })
      .execute();
    return !result.affected ? false : true;
  }

  async deleteTask(id: number): Promise<boolean> {
    const repository = await this._getRepository(TaskEntity);
    const result = await repository.delete(id);
    return !result.affected ? false : true;
  }
}
