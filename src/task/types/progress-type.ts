export enum TaskProgress {
  todo,
  inProgress,
  review,
  done,
}
