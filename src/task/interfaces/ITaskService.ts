import { TaskType } from '../types/task-type';
import { ICreateTask } from './ICreateTask';
import { ITask } from './ITask';
import { IUpdateAssignUser } from './IUpdateAssignUser';
import { IUpdateTask } from './IUpdateTask';

export interface ITaskService {
  create(taskData: ICreateTask, authorId: number, backlogId: number): Promise<ITask>;

  findAllTasksForBacklog(backlogId: number): Promise<ITask[]>;

  findAllTasksForSprint(sprintId: number): Promise<ITask[]>;

  findSpecificTypeTasksForSprint(sprintId: number, type: TaskType): Promise<ITask[]>;

  findSpecificTypeTasksForBacklog(backlogId: number, type: TaskType): Promise<ITask[]>;

  findOne(id: number): Promise<ITask>;

  update(id: number, updateTaskData: IUpdateTask): Promise<string>;

  updateAssignedUser(id: number, updates: IUpdateAssignUser): Promise<ITask>;

  assingTaskToSprint(taskId: number, sprintId: number): Promise<ITask>;

  remove(id: number): Promise<string>;
}
