import { IBacklog } from 'src/backlog/interfaces/IBacklog';
import { ISprint } from 'src/sprint/interfaces/sprint.interface';
import { IUser } from 'src/user/interfaces/IUser';
import { TaskPriority } from '../types/priority-type';
import { TaskProgress } from '../types/progress-type';
import { TaskType } from '../types/task-type';

export interface ITask {
  id?: number;
  type?: TaskType;
  name?: string;
  description?: string;
  progress?: TaskProgress;
  priority?: TaskPriority;
  estimatedTime?: number;
  authorId?: number;
  author?: IUser;
  userId?: number;
  user?: IUser;
  sprintId?: number;
  backlogId?: number;
  sprint?: ISprint;
  backlog?: IBacklog;
  createdAt?: Date;
  updatedAt?: Date;
}
