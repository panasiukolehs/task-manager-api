import { TaskPriority } from '../types/priority-type';
import { TaskProgress } from '../types/progress-type';
import { TaskType } from '../types/task-type';

export interface IUpdateTask {
  type?: TaskType;
  name?: string;
  description?: string;
  progress?: TaskProgress;
  priority?: TaskPriority;
  estimatedTimeMs?: number;
}
