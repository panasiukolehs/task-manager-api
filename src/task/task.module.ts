import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  diBacklogService,
  diMailService,
  diLoggerService,
  diRoleService,
  diTaskService,
  diUserProjectService,
  diUserService,
} from 'src/DIConfig/serviceConfig';
import { EncodingHepler } from 'src/shared/encoding/encodingHelper';
import {
  backlogRepositoryDAO,
  roleRepositoryDAO,
  taskRepositoryDAO,
  userProjectJucntionRepositoryDAO,
  userRepositoryDAO,
} from 'src/shared/dao/ConnectionDAO';
import { TaskController } from './controllers/task.controller';
import { TaskEntity } from './entities/task.entity';
import { MailTextHelper } from 'src/shared/helpers/mailTextHelper';

@Module({
  imports: [TypeOrmModule.forFeature([TaskEntity])],
  controllers: [TaskController],
  providers: [
    diTaskService,
    diUserService,
    diRoleService,
    diUserProjectService,
    diBacklogService,
    diMailService,
    diLoggerService,
    taskRepositoryDAO,
    userRepositoryDAO,
    roleRepositoryDAO,
    userProjectJucntionRepositoryDAO,
    backlogRepositoryDAO,
    EncodingHepler,
    MailTextHelper,
  ],
})
export class TaskModule {}
