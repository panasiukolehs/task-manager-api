import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Inject,
  Request,
  ParseIntPipe,
} from '@nestjs/common';
import { CreateTaskDto } from '../dto/create-task.dto';
import { UpdateTaskDto } from '../dto/update-task.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import {
  ApiBearerAuth,
  ApiBody,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  BACKLOG_SERVICE,
  MAIL_SERVICE,
  TASK_SERVICE,
  USER_PROJECT_SERVICE,
  USER_SERVICE,
} from 'src/constants/serviceConstants';
import { ITaskService } from '../interfaces/ITaskService';
import { ITask } from '../interfaces/ITask';
import { IUserRequestModel } from 'src/auth/interfaces/IUserRequestModel';
import { IUserService } from 'src/user/interfaces/IUserService';
import { TaskDto } from 'src/shared/swagger/dto/task.dto';
import { SingleErrorDto } from 'src/shared/swagger/dto/single-error.dto';
import { UpdateAssignUserDto } from '../dto/update-assign-user-dto';
import { IUserProjectService } from 'src/project/interfaces/IUserProjectService';
import { IBacklogService } from 'src/backlog/interfaces/IBacklogService';
import { IMailService } from 'src/mail/interfaces/IMailService';
import { MailTextHelper } from 'src/shared/helpers/mailTextHelper';

@UseGuards(JwtAuthGuard)
@ApiTags('tasks')
@Controller()
export class TaskController {
  constructor(
    @Inject(TASK_SERVICE)
    private readonly taskService: ITaskService,
    @Inject(USER_SERVICE)
    private readonly userService: IUserService,
    @Inject(USER_PROJECT_SERVICE)
    private readonly userProjectService: IUserProjectService,
    @Inject(BACKLOG_SERVICE)
    private readonly backlogService: IBacklogService,
    @Inject(MAIL_SERVICE)
    private readonly mailService: IMailService,
    private readonly mailTextHelper: MailTextHelper,
  ) {}

  @ApiBearerAuth()
  @ApiBody({
    type: CreateTaskDto,
  })
  @ApiCreatedResponse({
    description: 'Created task info',
    type: TaskDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Post('project/:projectId/task')
  async create(
    @Param('projectId', ParseIntPipe) projectId: number,
    @Request() req: IUserRequestModel,
    @Body() createTaskDto: CreateTaskDto,
  ): Promise<ITask> {
    const userBelongToProject = await this.userProjectService.checkIfUserBelongsToProject(
      req.user.id,
      projectId,
    );
    const backlog = await this.backlogService.findByProjectId(projectId);
    return await this.taskService.create(createTaskDto, req.user.id, backlog.id);
  }

  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Find a tasks for specific project',
    type: TaskDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Get('task/:id')
  async findOne(@Param('id', ParseIntPipe) id: number): Promise<ITask> {
    return await this.taskService.findOne(id);
  }

  @ApiBearerAuth()
  @ApiBody({
    type: UpdateTaskDto,
  })
  @ApiOkResponse({
    description: 'Successfully updated!',
    type: String,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiNotFoundResponse({
    description: "Task or provided parameters don't exist",
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Patch('task/:id')
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateTaskDto: UpdateTaskDto,
  ): Promise<string> {
    return await this.taskService.update(id, updateTaskDto);
  }

  @ApiBearerAuth()
  @ApiBody({
    type: UpdateTaskDto,
  })
  @ApiOkResponse({
    description: 'User assigned successfully',
    type: TaskDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiNotFoundResponse({
    description: "Task or user doesn't exist",
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Post('task/:id/assign-user')
  async updateAssignedUser(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateAssignUserto: UpdateAssignUserDto,
  ): Promise<ITask> {
    const user = await this.userService.findOne(updateAssignUserto.userId);
    const resultTask = await this.taskService.updateAssignedUser(id, updateAssignUserto);

    const mailText = this.mailTextHelper.generateUserAssignNotification(resultTask);
    await this.mailService.notifyUserOnMail(user.email, mailText);
    return resultTask;
  }

  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Successfully deleted!',
    type: String,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiNotFoundResponse({
    description: "Task doesn't exist",
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Delete('task/:id')
  async remove(@Param('id', ParseIntPipe) id: number): Promise<string> {
    return await this.taskService.remove(id);
  }
}
