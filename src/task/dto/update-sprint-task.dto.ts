import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { IUpdateSprintTask } from '../interfaces/IUpdateSprintTask';

export class UpdateSprintTaskDto implements IUpdateSprintTask {
  @ApiProperty()
  @IsInt()
  sprintId: number;
}
