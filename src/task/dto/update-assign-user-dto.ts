import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';
import { IUpdateAssignUser } from '../interfaces/IUpdateAssignUser';

export class UpdateAssignUserDto implements IUpdateAssignUser {
  @ApiProperty()
  @IsNumber()
  userId: number;
}
