import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsInt, IsNumber, IsOptional, IsString, Max, Min } from 'class-validator';
import { IUpdateTask } from '../interfaces/IUpdateTask';
import { TaskPriority } from '../types/priority-type';
import { TaskProgress } from '../types/progress-type';
import { TaskType } from '../types/task-type';

const INTEGER_MAX_VALUE = 2147483647;
export class UpdateTaskDto implements IUpdateTask {
  @ApiProperty()
  @IsOptional()
  @IsEnum(TaskType, { message: 'type must be a valid value (task, bug)' })
  type?: TaskType;

  @ApiProperty()
  @IsString()
  @IsOptional()
  name?: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  description?: string;

  @ApiProperty()
  @IsEnum(TaskProgress, { message: 'type must be a valid value (todo, inProgress, review, done)' })
  @IsOptional()
  progress?: TaskProgress;

  @ApiProperty()
  @IsEnum(TaskPriority, { message: 'type must be a valid value (low, medium, high, asap)' })
  @IsOptional()
  priority?: TaskPriority;

  @ApiProperty()
  @IsNumber()
  @Min(0)
  @Max(INTEGER_MAX_VALUE, { message: 'value should be in milliseconds and less than 2^31' })
  @IsOptional()
  estimatedTimeMs?: number;
}
