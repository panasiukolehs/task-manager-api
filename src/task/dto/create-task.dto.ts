import { ApiProperty } from '@nestjs/swagger';
import {
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  Min,
} from 'class-validator';
import { ICreateTask } from '../interfaces/ICreateTask';
import { TaskPriority } from '../types/priority-type';
import { TaskProgress } from '../types/progress-type';
import { TaskType } from '../types/task-type';

const INTEGER_MAX_VALUE = 2147483647;
export class CreateTaskDto implements ICreateTask {
  @ApiProperty()
  @IsEnum(TaskType, { message: 'type must be a valid value (task, bug)' })
  type: TaskType;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  description?: string;

  @ApiProperty()
  @IsEnum(TaskProgress, { message: 'type must be a valid value (todo, inProgress, review, done)' })
  @IsOptional()
  progress?: TaskProgress;

  @ApiProperty()
  @IsEnum(TaskPriority, { message: 'type must be a valid value (low, medium, high, asap)' })
  @IsOptional()
  priority?: TaskPriority;

  @ApiProperty()
  @IsNumber()
  @Min(0)
  @Max(INTEGER_MAX_VALUE, { message: 'value should be in milliseconds and less than 2^31' })
  @IsOptional()
  estimatedTimeMs?: number;

  @ApiProperty()
  @IsInt()
  @IsOptional()
  userId?: number;
}
