import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { taskRepositoryType } from 'src/shared/dao/ConnectionDAO';
import { RepositoryTypes } from 'src/shared/dao/types/repository-types.enum';
import { PINO_LOGGER_SERVICE } from '../../constants/serviceConstants';
import { PinoLoggerService } from '../../logger/services/pino-logger.service';
import { ICreateTask } from '../interfaces/ICreateTask';
import { ITask } from '../interfaces/ITask';
import { ITaskService } from '../interfaces/ITaskService';
import { IUpdateAssignUser } from '../interfaces/IUpdateAssignUser';
import { IUpdateTask } from '../interfaces/IUpdateTask';
import { TaskType } from '../types/task-type';

@Injectable()
export class TaskService implements ITaskService {
  constructor(
    @Inject(RepositoryTypes.TASK_REPOSITORY)
    private readonly taskRepository: taskRepositoryType,
    @Inject(PINO_LOGGER_SERVICE)
    private readonly logger: PinoLoggerService,
  ) {
    logger.setContext('TaskService');
  }

  async create(taskData: ICreateTask, authorId: number, backlogId: number): Promise<ITask> {
    return await this.taskRepository.createTask({ ...taskData, authorId, backlogId });
  }

  async findAllTasksForBacklog(backlogId: number): Promise<ITask[]> {
    const result = await this.taskRepository.findWithBacklogId(backlogId);
    if (!result.length) {
      this.logger.warn('Tasks which belong to such a backlog do not exist');
      throw new HttpException(
        'Tasks which belong to such a backlog do not exist',
        HttpStatus.NOT_FOUND,
      );
    }

    return result;
  }

  async findAllTasksForSprint(sprintId: number): Promise<ITask[]> {
    const result = await this.taskRepository.findWithSprintId(sprintId);
    if (!result.length) {
      this.logger.warn('Tasks which belong to such a sprint do not exist');
      throw new HttpException(
        'Tasks which belong to such a sprint do not exist',
        HttpStatus.NOT_FOUND,
      );
    }
    return result;
  }

  async findSpecificTypeTasksForSprint(sprintId: number, type: TaskType): Promise<ITask[]> {
    if (!(type in TaskType)) {
      this.logger.warn('Bad query type');
      throw new HttpException('Bad query type', HttpStatus.BAD_REQUEST);
    }
    const result = await this.taskRepository.findWithTypeForSprint(type, sprintId);
    if (!result.length) {
      this.logger.warn(`Tasks with type ${type} which belong to such a sprint do not exist`);
      throw new HttpException(
        `Tasks with type ${type} which belong to such a sprint do not exist`,
        HttpStatus.NOT_FOUND,
      );
    }

    return result;
  }

  async findSpecificTypeTasksForBacklog(backlogId: number, type: TaskType): Promise<ITask[]> {
    if (!(type in TaskType)) {
      this.logger.warn('Bad query type');
      throw new HttpException('Bad query type', HttpStatus.BAD_REQUEST);
    }
    const result = await this.taskRepository.findWithTypeForBacklog(type, backlogId);
    if (!result.length) {
      this.logger.warn(`Tasks with type ${type} which belong to such a backlog do not exist`);
      throw new HttpException(
        `Tasks with type ${type} which belong to such a backlog do not exist`,
        HttpStatus.NOT_FOUND,
      );
    }
    return result;
  }

  async findOne(id: number): Promise<ITask> {
    const result = await this.taskRepository.findById(id);
    if (!result) {
      this.logger.warn('Task with such an id does not exist');
      throw new HttpException('Task with such an id does not exist', HttpStatus.NOT_FOUND);
    }
    return result;
  }

  async update(id: number, updateTaskData: IUpdateTask): Promise<string> {
    const taskExist = await this.taskRepository.findById(id);
    if (!taskExist) {
      this.logger.warn('Task with such id does not exist');
      throw new HttpException('Task with such id does not exist', HttpStatus.NOT_FOUND);
    }
    const isUpdated = await this.taskRepository.updateTask(id, updateTaskData);
    if (!isUpdated) {
      this.logger.warn('Task has not been updated!');
      throw new HttpException('Task has not been updated!', HttpStatus.BAD_REQUEST);
    }
    return 'Successfully updated!';
  }

  async updateAssignedUser(id: number, updates: IUpdateAssignUser): Promise<ITask> {
    const taskExist = await this.taskRepository.findById(id);
    if (!taskExist) {
      this.logger.warn('Task with such id does not exist');
      throw new HttpException('Task with such id does not exist', HttpStatus.NOT_FOUND);
    }
    const isUpdated = await this.taskRepository.updateTask(id, updates);
    if (!isUpdated) {
      this.logger.warn('Task has not been updated!');
      throw new HttpException('Task has not been updated!', HttpStatus.BAD_REQUEST);
    }
    return await this.taskRepository.findById(id);
  }

  async assingTaskToSprint(taskId: number, sprintId: number): Promise<ITask> {
    const taskExist = await this.taskRepository.findById(taskId);
    if (!taskExist) {
      this.logger.warn('Task with such id does not exist');
      throw new HttpException('Task with such id does not exist', HttpStatus.NOT_FOUND);
    }
    const updatedTask: ITask = { sprintId, backlogId: null };
    const isUpdated = await this.taskRepository.updateTask(taskId, updatedTask);
    if (!isUpdated) {
      this.logger.warn('Task has not been updated!');
      throw new HttpException('Task has not been updated!', HttpStatus.BAD_REQUEST);
    }
    return await this.taskRepository.findById(taskId);
  }

  async remove(id: number): Promise<string> {
    const isDeleted = await this.taskRepository.deleteTask(id);
    if (!isDeleted) {
      this.logger.warn('Task with such an id does not exist');
      throw new HttpException('Task with such an id does not exist', HttpStatus.NOT_FOUND);
    }
    return 'Successfully deleted!';
  }
}
