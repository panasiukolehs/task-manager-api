import { Backlog } from 'src/backlog/entities/backlog.entity';
import { SprintEntity } from 'src/sprint/entities/sprint.entity';
import { User } from 'src/user/entities/user.entity';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ITask } from '../interfaces/ITask';
import { TaskPriority } from '../types/priority-type';
import { TaskProgress } from '../types/progress-type';
import { TaskType } from '../types/task-type';

@Entity({ name: 'tasks' })
export class TaskEntity implements ITask {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'enum',
    enum: TaskType,
    default: TaskType.task,
    nullable: false,
  })
  type: TaskType;

  @Column({ nullable: false })
  name: string;

  @Column()
  description: string;

  @Column({
    type: 'enum',
    enum: TaskProgress,
    default: TaskProgress.todo,
    nullable: false,
  })
  progress: TaskProgress;

  @Column({
    type: 'enum',
    enum: TaskPriority,
    default: TaskPriority.medium,
    nullable: false,
  })
  priority: TaskPriority;

  @Column({ name: 'estimated_time_ms' })
  estimatedTimeMs: number;

  @Column({ name: 'author_id', nullable: false })
  authorId: number;

  @Column({ name: 'user_id' })
  userId: number;

  @Column({ name: 'sprint_id' })
  sprintId: number;

  @Column({ name: 'backlog_id' })
  backlogId: number;

  @Column({ name: 'created_at' })
  createdAt: Date;

  @Column({ name: 'updated_at' })
  updatedAt: Date;

  @ManyToOne(() => User, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'author_id', referencedColumnName: 'id' })
  author?: User;

  @ManyToOne(() => User, { onDelete: 'SET NULL' })
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user?: User;

  @ManyToOne(() => SprintEntity, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'sprint_id', referencedColumnName: 'id' })
  sprint?: SprintEntity;

  @ManyToOne(() => Backlog, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'backlog_id', referencedColumnName: 'id' })
  backlog?: Backlog;
}
