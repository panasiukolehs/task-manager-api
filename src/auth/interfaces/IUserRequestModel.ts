import { Request } from 'express';
import { IUser } from 'src/user/interfaces/IUser';

export interface IUserRequestModel extends Request {
  user: IUser;
}
