import { IGetUser } from 'src/user/interfaces/IGetUser';

export interface ILoginResult {
  data: {
    user: IGetUser;
  };
  payload: {
    type: string;
    access_token: string;
    refresh_token?: string;
  };
}
