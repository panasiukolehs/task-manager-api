export class IPayloadData {
  id: number;
  email: string;
  roleName: string;
}
