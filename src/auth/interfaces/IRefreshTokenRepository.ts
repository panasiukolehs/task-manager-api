import { IUser } from '../../user/interfaces/IUser';

export interface IRefreshTokenRepository {
  id?: number;
  token: string | null;
  createdAt?: Date;
  updatedAt?: Date;
  user: IUser;
}
