import { ICreateUser } from 'src/user/interfaces/ICreateUser';
import { IUser } from 'src/user/interfaces/IUser';
import { ILoginResult } from './ILoginResult';

export interface IAuthService {
  validateUser(email: string, password: string): Promise<IUser>;

  login(user: IUser): Promise<ILoginResult>;

  register(userData: ICreateUser, role: string): Promise<ILoginResult>;

  logout(userId: number): Promise<string>;

  generateAccessTokenFromRefresh(user: IUser): Promise<ILoginResult>;
}
