export class ITokenPayload {
  sub: number;
  email: string;
  roleName: string;
}
