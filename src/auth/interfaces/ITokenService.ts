import { IUser } from 'src/user/interfaces/IUser';
import { RefreshToken } from '../entities/refresh-token.entity';

export interface ITokenService {
  generateAccessToken(payload): Promise<string>;

  generateRefreshToken(payload): Promise<string>;

  hashToken(refreshToken: string): Promise<string>;

  returnUserIfTokenValid(refreshToken: string, userId: number): Promise<IUser>;

  saveToken(refreshToken: string, userId: number): Promise<RefreshToken>;

  findToken(userId: number): Promise<RefreshToken>;

  deleteToken(userId: number): Promise<string>;
}
