import { Body, Controller, HttpCode, Inject, Post, Request, UseGuards } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import { CreateUserDto } from 'src/user/dto/create-user.dto';
import { ILoginResult } from '../interfaces/ILoginResult';
import { IUserRequestModel } from '../interfaces/IUserRequestModel';
import { JwtRefreshAuthGuard } from '../jwt-refresh-auth.guard';
import { LocalAuthGuard } from '../local-auth.guard';
import { AuthService } from '../services/auth.service';
import {
  AuthResponseIncludeActiveDto,
  AuthResponseIncludeRoleDto,
} from '../../shared/swagger/dto/auth-response.dto';
import { MultipleErrorDto } from '../../shared/swagger/dto/multiple-error.dto';
import { RefreshTokenDto } from '../dto/refreshToken.dto';
import { SingleErrorDto } from '../../shared/swagger/dto/single-error.dto';
import { PROJECT_MANAGER } from 'src/constants/roleConstants';

const AUTH_SERVICE = 'AUTH_SERVICE';
@ApiTags('auth')
@Controller()
export class AuthController {
  constructor(
    @Inject(AUTH_SERVICE)
    private readonly authService: AuthService,
  ) {}

  @ApiBadRequestResponse({
    description: 'The data is inconsistent',
    type: MultipleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @ApiCreatedResponse({
    description: 'Registration is succeed',
    type: AuthResponseIncludeRoleDto,
  })
  @ApiBody({
    description: 'Credentials to create a user',
    type: CreateUserDto,
  })
  @Post('register')
  async signup(@Body() createUserDto: CreateUserDto): Promise<ILoginResult> {
    return await this.authService.register(createUserDto, PROJECT_MANAGER);
  }

  @ApiBody({
    type: CreateUserDto,
  })
  @ApiCreatedResponse({
    description: 'Authorization is succeed',
    type: AuthResponseIncludeActiveDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid credentials',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() req: IUserRequestModel): Promise<ILoginResult> {
    return await this.authService.login(req.user);
  }

  @ApiBody({
    type: RefreshTokenDto,
  })
  @ApiOkResponse({
    description: 'Token is refreshed',
    type: AuthResponseIncludeActiveDto,
  })
  @ApiBadRequestResponse({
    description: 'Refresh token malformed',
    type: SingleErrorDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid credentials',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @HttpCode(200)
  @UseGuards(JwtRefreshAuthGuard)
  @Post('refresh-token')
  async refreshToken(@Request() req: IUserRequestModel): Promise<ILoginResult> {
    return await this.authService.generateAccessTokenFromRefresh(req.user);
  }

  @ApiBody({
    type: RefreshTokenDto,
  })
  @ApiOkResponse({
    description: 'Logged out successfully',
    type: String,
  })
  @ApiBadRequestResponse({
    description: 'Refresh token malformed',
    type: SingleErrorDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @HttpCode(200)
  @UseGuards(JwtRefreshAuthGuard)
  @Post('logout')
  async logout(@Request() req: IUserRequestModel): Promise<string> {
    return await this.authService.logout(req.user.id);
  }
}
