import { Inject, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Request } from 'express';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { TOKEN_SERVICE } from 'src/constants/serviceConstants';
import { IUser } from 'src/user/interfaces/IUser';
import { jwtConstant } from '../../constants/tokenConstants';
import { ITokenPayload } from '../interfaces/ITokenPayload';
import { TokenService } from './token.service';

@Injectable()
export class JwtRefreshStrategy extends PassportStrategy(Strategy, 'jwt-refresh-token') {
  constructor(
    @Inject(TOKEN_SERVICE)
    private readonly tokenService: TokenService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromBodyField('refresh_token'),
      ignoreExpiration: false,
      secretOrKey: jwtConstant.secret,
      passReqToCallback: true,
    });
  }

  async validate(req: Request, payload: ITokenPayload): Promise<IUser> {
    return await this.tokenService.returnUserIfTokenValid(req.body.refresh_token, payload.sub);
  }
}
