import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { jwtConstant } from '../../constants/tokenConstants';
import { IPayloadData } from '../interfaces/IPayloadData';
import { ITokenPayload } from '../interfaces/ITokenPayload';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstant.secret,
    });
  }

  validate(payload: ITokenPayload): IPayloadData {
    return { id: payload.sub, email: payload.email, roleName: payload.roleName };
  }
}
