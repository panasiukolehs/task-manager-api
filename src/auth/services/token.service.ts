import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { RefreshToken } from '../entities/refresh-token.entity';
import { jwtConstant } from '../../constants/tokenConstants';
import { JwtService } from '@nestjs/jwt';
import { IUserService } from 'src/user/interfaces/IUserService';
import { EncodingHepler } from 'src/shared/encoding/encodingHelper';
import { IUser } from 'src/user/interfaces/IUser';
import { ITokenService } from '../interfaces/ITokenService';
import { PINO_LOGGER_SERVICE, USER_SERVICE } from 'src/constants/serviceConstants';
import { RepositoryTypes } from '../../shared/dao/types/repository-types.enum';
import { refreshTokenRepositoryType } from '../../shared/dao/ConnectionDAO';
import { ITokenPayload } from '../interfaces/ITokenPayload';
import { PinoLoggerService } from '../../logger/services/pino-logger.service';

@Injectable()
export class TokenService implements ITokenService {
  constructor(
    @Inject(RepositoryTypes.TOKEN_REPOSITORY)
    private readonly tokenRepository: refreshTokenRepositoryType,
    private jwtService: JwtService,
    @Inject(USER_SERVICE)
    private readonly userService: IUserService,
    private readonly encoder: EncodingHepler,
    @Inject(PINO_LOGGER_SERVICE)
    private readonly logger: PinoLoggerService,
  ) {
    logger.setContext('TokenService');
  }

  async generateAccessToken(payload: ITokenPayload): Promise<string> {
    return await this.jwtService.signAsync(payload);
  }

  async generateRefreshToken(payload: ITokenPayload): Promise<string> {
    return await this.jwtService.signAsync(payload, {
      expiresIn: jwtConstant.refreshSecretExpiration,
      algorithm: 'HS256',
    });
  }

  async hashToken(refreshToken: string): Promise<string> {
    return await this.encoder.hashData(refreshToken, 10);
  }

  async returnUserIfTokenValid(refreshToken: string, userId: number): Promise<IUser> {
    const savedToken = await this.findToken(userId);
    if (!savedToken.token) {
      this.logger.warn('Refresh token malformed');
      throw new HttpException('Refresh token malformed', HttpStatus.BAD_REQUEST);
    }

    const matchingResult = await this.encoder.compareData(refreshToken, savedToken.token);

    if (!matchingResult) {
      this.logger.warn('Refresh token malformed');
      throw new HttpException('Refresh token malformed', HttpStatus.BAD_REQUEST);
    }
    return await this.userService.findOne(userId);
  }

  async saveToken(refreshToken: string, userId: number): Promise<RefreshToken> {
    const encryptedToken = await this.hashToken(refreshToken);
    const user = await this.userService.findOne(userId);
    const tokenData = await this.findToken(userId);
    if (!tokenData)
      return await this.tokenRepository.createToken({ token: encryptedToken, user: user });
    return await this.tokenRepository.updateToken({
      id: tokenData.id,
      token: encryptedToken,
      user: user,
    });
  }

  async findToken(userId: number): Promise<RefreshToken> {
    const user = await this.userService.findOne(userId);
    return await this.tokenRepository.findByUser(user);
  }

  async deleteToken(userId: number): Promise<string> {
    const user = await this.userService.findOne(userId);
    const token = await this.tokenRepository.findByUser(user);
    if (!token) {
      this.logger.warn('Token not found');
      throw new HttpException('Token not found', HttpStatus.NOT_FOUND);
    }
    Object.assign(token, { userId: user, token: null });
    await this.tokenRepository.updateToken(token);
    return 'Successfully deleted!';
  }
}
