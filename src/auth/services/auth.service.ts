import { Inject, Injectable } from '@nestjs/common';
import { IUser } from 'src/user/interfaces/IUser';
import { IUserService } from 'src/user/interfaces/IUserService';
import { ICreateUser } from 'src/user/interfaces/ICreateUser';
import { TokenService } from './token.service';
import { IAuthService } from '../interfaces/IAuthService';
import { EncodingHepler } from 'src/shared/encoding/encodingHelper';
import { mapUser } from 'src/user/mappers/getUserMapper';
import { ILoginResult } from '../interfaces/ILoginResult';
import { IGetUser } from 'src/user/interfaces/IGetUser';
import { TOKEN_SERVICE, USER_SERVICE } from 'src/constants/serviceConstants';
import { ITokenPayload } from '../interfaces/ITokenPayload';

@Injectable()
export class AuthService implements IAuthService {
  constructor(
    @Inject(USER_SERVICE)
    private readonly userService: IUserService,
    @Inject(TOKEN_SERVICE)
    private readonly tokenService: TokenService,
    private readonly encoder: EncodingHepler,
  ) {}

  async validateUser(email: string, password: string): Promise<IUser> {
    const user: IUser = await this.userService.findOneByEmail(email);

    if (user) {
      const result: boolean = await this.encoder.compareData(password, user.password);
      if (result) {
        return mapUser(user);
      }
    }
    return null;
  }

  async login(user: IUser): Promise<ILoginResult> {
    const payload: ITokenPayload = {
      email: user.email,
      sub: user.id,
      roleName: user.role.roleName,
    };
    const token = await this.tokenService.generateAccessToken(payload);
    const refreshToken = await this.tokenService.generateRefreshToken(payload);

    await this.tokenService.saveToken(refreshToken, user.id);
    return {
      data: {
        user: user,
      },
      payload: {
        type: 'Bearer',
        access_token: token,
        refresh_token: refreshToken,
      },
    };
  }

  async register(userData: ICreateUser, role: string): Promise<ILoginResult> {
    const user: IGetUser = await this.userService.create(userData, role);
    const payload: ITokenPayload = {
      email: user.email,
      sub: user.id,
      roleName: user.role.roleName,
    };

    const token = await this.tokenService.generateAccessToken(payload);
    const refreshToken = await this.tokenService.generateRefreshToken(payload);

    await this.tokenService.saveToken(refreshToken, user.id);
    return {
      data: {
        user: user,
      },
      payload: {
        type: 'Bearer',
        access_token: token,
        refresh_token: refreshToken,
      },
    };
  }

  async logout(userId: number): Promise<string> {
    return await this.tokenService.deleteToken(userId);
  }

  async generateAccessTokenFromRefresh(user: IUser): Promise<ILoginResult> {
    const payload: ITokenPayload = {
      email: user.email,
      sub: user.id,
      roleName: user.role.roleName,
    };
    const newToken = await this.tokenService.generateAccessToken(payload);
    return {
      data: {
        user: user,
      },
      payload: {
        type: 'Bearer',
        access_token: newToken,
      },
    };
  }
}
