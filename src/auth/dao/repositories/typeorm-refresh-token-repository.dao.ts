import { Injectable } from '@nestjs/common';

import { RepositoryDAO } from '../../../shared/dao/repositories/typeorm-repository.dao';
import { RefreshToken as RefreshTokenEntity } from '../../entities/refresh-token.entity';
import { IRepository } from '../../../shared/dao/types/base-repository.interface';
import { IRefreshTokenRepository } from '../../interfaces/IRefreshTokenRepository';
import { IUser } from '../../../user/interfaces/IUser';

@Injectable()
export class TypeormRefreshTokenRepositoryDAO
  extends RepositoryDAO<RefreshTokenEntity>
  implements IRepository<RefreshTokenEntity>
{
  constructor() {
    super();
  }

  async find(): Promise<RefreshTokenEntity[]> {
    const repository = await this._getRepository(RefreshTokenEntity);
    return await repository.find();
  }

  async findById(id: number): Promise<RefreshTokenEntity> {
    const repository = await this._getRepository(RefreshTokenEntity);
    return await repository.findOne(id);
  }

  async findByUser(user: IUser): Promise<RefreshTokenEntity> {
    const repository = await this._getRepository(RefreshTokenEntity);
    return await repository.findOne({ user: user });
  }

  async createToken(newToken: IRefreshTokenRepository): Promise<RefreshTokenEntity> {
    const repository = await this._getRepository(RefreshTokenEntity);
    const token = new RefreshTokenEntity();
    Object.assign(token, newToken);
    return await repository.save(token);
  }

  async updateToken(tokenUpdates: IRefreshTokenRepository): Promise<RefreshTokenEntity> {
    const repository = await this._getRepository(RefreshTokenEntity);
    const token = await this.findByUser(tokenUpdates.user);
    Object.assign(token, tokenUpdates);
    return await repository.save(tokenUpdates);
  }
}
