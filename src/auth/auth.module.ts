import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  diAauthService,
  diLoggerService,
  diRoleService,
  diTokenService,
  diUserService,
} from 'src/DIConfig/serviceConfig';
import { EncodingHepler } from 'src/shared/encoding/encodingHelper';
import { Role } from 'src/role/entities/role.entity';
import { User } from 'src/user/entities/user.entity';
import { jwtConstant } from '../constants/tokenConstants';
import { AuthController } from './controllers/auth.controller';
import { RefreshToken } from './entities/refresh-token.entity';
import { JwtRefreshStrategy } from './services/jwt-refresh.strategy';
import { JwtStrategy } from './services/jwt.strategy';
import { LocalStrategy } from './services/local.strategy';
import {
  roleRepositoryDAO,
  userRepositoryDAO,
  tokenRepositoryDAO,
} from '../shared/dao/ConnectionDAO';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, RefreshToken, Role]),
    PassportModule,
    JwtModule.register({
      secret: jwtConstant.secret,
      signOptions: {
        expiresIn: jwtConstant.secretExpiration,
        algorithm: 'HS256',
      },
    }),
  ],
  controllers: [AuthController],
  providers: [
    diAauthService,
    diUserService,
    diRoleService,
    diTokenService,
    EncodingHepler,
    LocalStrategy,
    JwtStrategy,
    JwtRefreshStrategy,
    userRepositoryDAO,
    roleRepositoryDAO,
    tokenRepositoryDAO,
    diLoggerService,
  ],
  exports: [diAauthService],
})
export class AuthModule {}
