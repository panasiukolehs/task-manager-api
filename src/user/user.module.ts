import { Module } from '@nestjs/common';
import { UserController } from './controllers/user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { diLoggerService, diRoleService, diUserService } from 'src/DIConfig/serviceConfig';
import { EncodingHepler } from 'src/shared/encoding/encodingHelper';
import { Role } from 'src/role/entities/role.entity';
import { roleRepositoryDAO, userRepositoryDAO } from '../shared/dao/ConnectionDAO';

@Module({
  imports: [TypeOrmModule.forFeature([User, Role])],
  controllers: [UserController],
  providers: [
    diUserService,
    diRoleService,
    EncodingHepler,
    userRepositoryDAO,
    roleRepositoryDAO,
    diLoggerService,
  ],
  exports: [diUserService, userRepositoryDAO],
})
export class UserModule {}
