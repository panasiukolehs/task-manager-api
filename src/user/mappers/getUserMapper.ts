import { IGetUser } from '../interfaces/IGetUser';
import { IUser } from '../interfaces/IUser';

export const mapUser = (user: IUser): IGetUser => {
  return {
    id: user.id,
    email: user.email,
    role: user.role,
    roleId: user.roleId,
    active: user.active,
    createdAt: user.createdAt,
    updatedAt: user.updatedAt,
  };
};
