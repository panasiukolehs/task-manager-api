import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { IRole } from '../../role/interfaces/IRole';
import { IUser } from '../interfaces/IUser';
import { Role } from '../../role/entities/role.entity';
@Entity({ name: 'users' })
export class User implements IUser {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true, nullable: false })
  email: string;

  @Column({ nullable: false, select: false })
  password?: string;

  @Column()
  active: boolean;

  @Column({ name: 'created_at' })
  createdAt?: Date;

  @Column({ name: 'updated_at' })
  updatedAt?: Date;

  @Column({ name: 'role_id' })
  roleId: number;

  @ManyToOne(() => Role, { eager: true })
  @JoinColumn({ name: 'role_id', referencedColumnName: 'id' })
  role: IRole;
}
