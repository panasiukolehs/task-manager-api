import { PartialType } from '@nestjs/mapped-types';
import { IUpdateUser } from '../interfaces/IUpdateUser';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto extends PartialType(CreateUserDto) implements IUpdateUser {}
