import { IsEmail, IsNotEmpty, Matches } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

import { ICreateUser } from '../interfaces/ICreateUser';

export class CreateUserDto implements ICreateUser {
  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @Matches(/((?=.*[a-z])(?=.*[A-Z])(?=.*\d)).{5,}/, {
    message: 'Password is too weak, at least 1 big letter and 1 number',
  })
  password: string;
}
