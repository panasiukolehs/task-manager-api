import { IRole } from 'src/role/interfaces/IRole';

export interface IGetUser {
  id: number;
  email: string;
  role: IRole;
  roleId: number;
  active: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}
