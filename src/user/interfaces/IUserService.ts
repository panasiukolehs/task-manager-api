import { ICreateUser } from './ICreateUser';
import { IGetUser } from './IGetUser';
import { IUpdateUser } from './IUpdateUser';
import { IUser } from './IUser';

export interface IUserService {
  create(userData: ICreateUser, role: string): Promise<IGetUser>;

  hashPassword(password: string): Promise<string>;

  findAll(): Promise<IUser[]>;

  findOne(id: number): Promise<IUser>;

  findOneByEmail(email: string): Promise<IUser>;

  update(id: number, userData: IUpdateUser): Promise<void>;

  remove(id: number): Promise<void>;
}
