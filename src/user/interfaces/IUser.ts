import { IRole } from '../../role/interfaces/IRole';

export interface IUser {
  id: number;
  email: string;
  password?: string;
  roleId: number;
  role: IRole;
  active: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}
