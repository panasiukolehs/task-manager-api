import { IRole } from '../../role/interfaces/IRole';

export interface IUserRepository {
  id?: number;
  email?: string;
  password?: string;
  role?: IRole;
  roleId?: number;
  active?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}
