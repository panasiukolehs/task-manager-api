import { IUser } from './IUser';

export type UserType = Omit<IUser, 'password' | 'createdAt' | 'updatedAt'> & {
  password: string;
  createdAt: Date;
  updatedAt: Date;
};
