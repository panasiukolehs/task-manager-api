export interface ICreateUser {
  email: string;
  password: string;
  active?: boolean;
}
