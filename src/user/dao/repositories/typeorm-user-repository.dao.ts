import { DeleteResult } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';

import { RepositoryDAO } from '../../../shared/dao/repositories/typeorm-repository.dao';
import { IRepository } from '../../../shared/dao/types/base-repository.interface';
import { IUserRepository } from '../../interfaces/IUserRepository';
import { User as UserEntity } from '../../entities/user.entity';

@Injectable()
export class TypeormUserRepositoryDAO
  extends RepositoryDAO<UserEntity>
  implements IRepository<UserEntity>
{
  constructor() {
    super();
  }

  async find(): Promise<UserEntity[]> {
    const repository = await this._getRepository(UserEntity);
    return await repository.find({ select: ['id', 'email', 'role', 'active'] });
  }

  async findById(id: number): Promise<UserEntity> {
    const repository = await this._getRepository(UserEntity);
    return await repository.findOne(id, { select: ['id', 'email', 'role', 'active'] });
  }

  async findByIdWithPassword(id: number): Promise<UserEntity> {
    const repository = await this._getRepository(UserEntity);
    return await repository.findOne(id, {
      select: ['id', 'email', 'role', 'active', 'password'],
    });
  }

  async findByEmail(email: string): Promise<UserEntity> {
    const repository = await this._getRepository(UserEntity);
    return await repository.findOne({ email }, { select: ['id', 'email', 'role', 'active'] });
  }

  async findByEmailWithPassword(email: string): Promise<UserEntity> {
    const repository = await this._getRepository(UserEntity);
    return await repository.findOne(
      { email },
      { select: ['id', 'email', 'role', 'active', 'password'] },
    );
  }

  async findByParams(params: IUserRepository): Promise<UserEntity[]> {
    const repository = await this._getRepository(UserEntity);
    return await repository.find(params);
  }

  async createUser(newUser: IUserRepository): Promise<UserEntity> {
    const repository = await this._getRepository(UserEntity);
    const user = new UserEntity();
    Object.assign(user, newUser);
    return await repository.save(user);
  }

  async updateUser(userUpdates: IUserRepository): Promise<UserEntity> {
    const repository = await this._getRepository(UserEntity);
    return await repository.save(userUpdates);
  }

  async deleteUser(id: number): Promise<boolean> {
    const repository = await this._getRepository(UserEntity);
    const result = await repository.delete(id);
    return !result.affected ? false : true;
  }
}
