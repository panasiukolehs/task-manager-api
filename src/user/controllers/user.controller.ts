import {
  Controller,
  Get,
  Body,
  Patch,
  Param,
  Delete,
  Inject,
  UseGuards,
  Request,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import { UpdateUserDto } from '../dto/update-user.dto';
import { IUserService } from '../interfaces/IUserService';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { SingleErrorDto } from '../../shared/swagger/dto/single-error.dto';
import { UserIncludeActiveDto } from '../../shared/swagger/dto/user.dto';
import { UpdatesUserDto } from '../../shared/swagger/dto/updates-user.dto';
import { IUserRequestModel } from '../../auth/interfaces/IUserRequestModel';
import { IUser } from '../interfaces/IUser';
import { Roles } from 'src/role-based-auth/decorators/role.decorator';
import { PROJECT_MANAGER } from 'src/constants/roleConstants';
import { USER_SERVICE } from 'src/constants/serviceConstants';
import { RolesGuard } from 'src/role-based-auth/roles.guard';

@UseGuards(JwtAuthGuard)
@ApiTags('users')
@Controller('user')
export class UserController {
  constructor(
    @Inject(USER_SERVICE)
    private readonly userService: IUserService,
  ) {}

  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Authenticated user info',
    type: UserIncludeActiveDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Get()
  async findOne(@Request() req: IUserRequestModel): Promise<IUser> {
    return await this.userService.findOne(req.user.id);
  }

  @ApiBearerAuth()
  @ApiBody({
    type: UpdatesUserDto,
  })
  @ApiOkResponse({
    description: 'Successfully updated',
    type: String,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiNotFoundResponse({
    description: "User doesn't exist",
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Patch()
  async update(
    @Request() req: IUserRequestModel,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<string> {
    await this.userService.update(req.user.id, updateUserDto);
    return 'Successfully updated!';
  }

  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'User has successfully deleted',
    type: String,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token',
    type: SingleErrorDto,
  })
  @ApiNotFoundResponse({
    description: "User doesn't exist",
    type: SingleErrorDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: SingleErrorDto,
  })
  @Roles(PROJECT_MANAGER)
  @UseGuards(RolesGuard)
  @Delete(':id')
  async remove(@Param('id') id: string): Promise<string> {
    await this.userService.remove(+id);
    return 'Successfully deleted!';
  }
}
