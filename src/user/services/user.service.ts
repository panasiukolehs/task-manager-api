import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';

import { EncodingHepler } from 'src/shared/encoding/encodingHelper';
import { ICreateUser } from '../interfaces/ICreateUser';
import { IRole } from '../../role/interfaces/IRole';
import { IUpdateUser } from '../interfaces/IUpdateUser';
import { IUser } from '../interfaces/IUser';
import { IUserService } from '../interfaces/IUserService';
import { RoleService } from 'src/role/services/role.service';
import { IGetUser } from '../interfaces/IGetUser';
import { mapUser } from '../mappers/getUserMapper';
import { PINO_LOGGER_SERVICE, ROLE_SERVICE } from 'src/constants/serviceConstants';
import { RepositoryTypes } from '../../shared/dao/types/repository-types.enum';
import { userRepositoryType } from '../../shared/dao/ConnectionDAO';
import { PinoLoggerService } from '../../logger/services/pino-logger.service';

@Injectable()
export class UserService implements IUserService {
  constructor(
    @Inject(RepositoryTypes.USER_REPOSITORY)
    private userRepository: userRepositoryType,
    @Inject(ROLE_SERVICE)
    private roleService: RoleService,
    private encoder: EncodingHepler,
    @Inject(PINO_LOGGER_SERVICE)
    private readonly logger: PinoLoggerService,
  ) {
    logger.setContext('UserService');
  }

  async hashPassword(password: string): Promise<string> {
    return await this.encoder.hashData(password, 10);
  }

  async create(userData: ICreateUser, role: string): Promise<IGetUser> {
    const userExist = await this.userRepository.findByEmail(userData.email);
    if (userExist) {
      this.logger.warn('User already exists');
      throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
    }

    const userRole = await this.roleService.getRoleByName(role);

    userData.password = await this.hashPassword(userData.password);
    const user: IUser = await this.userRepository.createUser({ ...userData, role: userRole });
    return mapUser(user);
  }

  async findOneByEmail(email: string): Promise<IUser> {
    return await this.userRepository.findByEmailWithPassword(email);
  }

  async findOne(id: number): Promise<IUser> {
    const result = await this.userRepository.findById(id);
    if (!result) {
      this.logger.warn("User doesn't exist!");
      throw new HttpException("User doesn't exist!", HttpStatus.NOT_FOUND);
    }
    return result;
  }

  async findAll(): Promise<IUser[]> {
    return await this.userRepository.find();
  }

  async update(id: number, userData: IUpdateUser): Promise<void> {
    const user = await this.userRepository.findById(id);
    if (!user) {
      this.logger.warn("A user with specified ID hasn't found");
      throw new HttpException("A user with specified ID hasn't found", HttpStatus.NOT_FOUND);
    }
    const emailExists = await this.userRepository.findByEmail(userData.email);
    if (emailExists) {
      this.logger.warn('A user with such an email already exists');
      throw new HttpException('A user with such an email already exists', HttpStatus.BAD_REQUEST);
    }
    if (userData.password) {
      userData.password = await this.hashPassword(userData.password);
    }
    await this.userRepository.updateUser({ id, ...userData });
  }

  async remove(id: number): Promise<void> {
    const isDeleted = await this.userRepository.deleteUser(id);
    if (!isDeleted) {
      this.logger.warn("User doesn't exist!");
      throw new HttpException("User doesn't exist!", HttpStatus.NOT_FOUND);
    }
  }
}
