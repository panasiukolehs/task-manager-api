import { Inject, Injectable } from '@nestjs/common';

import { IRole } from '../interfaces/IRole';
import { IRoleService } from '../interfaces/IRoleService';
import { RepositoryTypes } from '../../shared/dao/types/repository-types.enum';
import { roleRepositoryType } from '../../shared/dao/ConnectionDAO';

@Injectable()
export class RoleService implements IRoleService {
  constructor(
    @Inject(RepositoryTypes.ROLE_REPOSITORY)
    private roleRepository: roleRepositoryType,
  ) {}

  async getRoleByName(name: string): Promise<IRole> {
    return await this.roleRepository.findByName(name);
  }

  async getRoleById(roleId: number): Promise<IRole> {
    return await this.roleRepository.findById(roleId);
  }
}
