import { IRole } from './IRole';

export interface IRoleService {
  getRoleByName(name: string): Promise<IRole>;
  getRoleById(roleId: number): Promise<IRole>;
}
