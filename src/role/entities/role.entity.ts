import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IRole } from '../interfaces/IRole';

@Entity({ name: 'roles' })
export class Role implements IRole {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'role_name' })
  roleName: string;
}
