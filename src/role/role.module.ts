import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { diRoleService } from 'src/DIConfig/serviceConfig';
import { Role } from './entities/role.entity';
import { roleRepositoryDAO } from '../shared/dao/ConnectionDAO';

@Module({
  imports: [TypeOrmModule.forFeature([Role])],
  providers: [diRoleService, roleRepositoryDAO],
  exports: [diRoleService, roleRepositoryDAO],
})
export class RoleModule {}
