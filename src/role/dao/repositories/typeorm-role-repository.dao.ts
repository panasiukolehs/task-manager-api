import { Injectable } from '@nestjs/common';

import { RepositoryDAO } from '../../../shared/dao/repositories/typeorm-repository.dao';
import { Role as RoleEntity } from '../../entities/role.entity';
import { IRepository } from '../../../shared/dao/types/base-repository.interface';

@Injectable()
export class TypeormRoleRepositoryDAO
  extends RepositoryDAO<RoleEntity>
  implements IRepository<RoleEntity>
{
  constructor() {
    super();
  }

  async find(): Promise<RoleEntity[]> {
    const repository = await this._getRepository(RoleEntity);
    return await repository.find();
  }

  async findById(id: number): Promise<RoleEntity> {
    const repository = await this._getRepository(RoleEntity);
    return await repository.findOne(id);
  }

  async findByName(name: string): Promise<RoleEntity> {
    const repository = await this._getRepository(RoleEntity);
    return await repository.findOne({ roleName: name });
  }
}
