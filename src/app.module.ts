import { MiddlewareConsumer, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';

import ormconfig from '../config/ormconfig';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { ProjectModule } from './project/project.module';
import { AuthModule } from './auth/auth.module';
import { RoleBasedAuthModule } from './role-based-auth/role-based-auth.module';
import { TaskModule } from './task/task.module';
import { SprintModule } from './sprint/sprint.module';
import { BacklogModule } from './backlog/backlog.module';
import { InviteModule } from './invite/invite.module';
import { MailModule } from './mail/mail.module';
import { LoggerModule } from './logger/logger.module';
import LoggerMiddleware from './logger/middlewares/logger.middleware';

@Module({
  imports: [
    ConfigModule.forRoot({
      expandVariables: true,
    }),
    TypeOrmModule.forRoot(ormconfig),
    ScheduleModule.forRoot(),
    UserModule,
    ProjectModule,
    AuthModule,
    RoleBasedAuthModule,
    TaskModule,
    SprintModule,
    BacklogModule,
    InviteModule,
    MailModule,
    LoggerModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
