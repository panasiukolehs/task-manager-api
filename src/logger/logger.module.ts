import { Module } from '@nestjs/common';
import { PINO_LOGGER_SERVICE } from '../constants/serviceConstants';
import { diLoggerService } from '../DIConfig/serviceConfig';

@Module({
  providers: [diLoggerService],
  exports: [PINO_LOGGER_SERVICE],
})
export class LoggerModule {}
