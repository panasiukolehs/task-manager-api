import { Injectable, LoggerService } from '@nestjs/common';
import pinoLogger from 'pino';

@Injectable()
export class PinoLoggerService implements LoggerService {
  private pino = pinoLogger({
    prettyPrint: true,
  });

  private context?: string;

  private getMessage(message: string, context?: string): string {
    return context ? `[ ${context} ] ${message}` : message;
  }

  setContext(context: string): void {
    this.context = context;
  }

  log(message: string, context?: string): void {
    this.pino.info(this.getMessage(message, context || this.context));
  }
  error(message: string, trace?: string, context?: string): void {
    this.pino.error(this.getMessage(message, context || this.context));
    if (trace) {
      this.pino.error(trace);
    }
  }
  warn(message: string, context?: string): void {
    this.pino.warn(this.getMessage(message, context || this.context));
  }
}
