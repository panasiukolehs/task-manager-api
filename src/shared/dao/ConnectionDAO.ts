import { RepositoryTypes } from './types/repository-types.enum';
import { TypeormUserRepositoryDAO } from '../../user/dao/repositories/typeorm-user-repository.dao';
import { TypeormRoleRepositoryDAO } from '../../role/dao/repositories/typeorm-role-repository.dao';
import { TypeormRefreshTokenRepositoryDAO } from '../../auth/dao/repositories/typeorm-refresh-token-repository.dao';
import { TypeormTaskRepositoryDAO } from 'src/task/dao/typeorm-task-repository.dao';
import { TypeormSprintRepositoryDAO } from '../../sprint/dao/repository/typeorm-sprint-repository.dao';
import { TypeormProjectRepositoryDAO } from '../../project/dao/repositories/typeorm-project-repository.dao';
import { TypeormBacklogRepositoryDAO } from '../../backlog/dao/repositories/typeorm-backlog-repository.dao';
import { TypeormUserProjectJunctionRepositoryDAO } from '../../project/dao/repositories/typeorm-project-user-junction-repository.dao';
import { TypeormInviteRepositoryDAO } from '../../invite/dao/repositories/typeorm-invite-repository.dao';
import { TypeormInvitesUsersRolesProjectsCoherenceRepositoryDao } from '../../invite/dao/repositories/typeorm-invites-users-roles-projects-coherence-repository.dao';

export const userRepositoryDAO = {
  provide: RepositoryTypes.USER_REPOSITORY,
  useClass: TypeormUserRepositoryDAO,
};
export type userRepositoryType = TypeormUserRepositoryDAO;

export const roleRepositoryDAO = {
  provide: RepositoryTypes.ROLE_REPOSITORY,
  useClass: TypeormRoleRepositoryDAO,
};
export type roleRepositoryType = TypeormRoleRepositoryDAO;

export const tokenRepositoryDAO = {
  provide: RepositoryTypes.TOKEN_REPOSITORY,
  useClass: TypeormRefreshTokenRepositoryDAO,
};
export type refreshTokenRepositoryType = TypeormRefreshTokenRepositoryDAO;

export const taskRepositoryDAO = {
  provide: RepositoryTypes.TASK_REPOSITORY,
  useClass: TypeormTaskRepositoryDAO,
};
export type taskRepositoryType = TypeormTaskRepositoryDAO;

export const sprintRepositoryDAO = {
  provide: RepositoryTypes.SPRINT_REPOSITORY,
  useClass: TypeormSprintRepositoryDAO,
};
export type sprintRepositoryType = TypeormSprintRepositoryDAO;

export const projectRepositoryDAO = {
  provide: RepositoryTypes.PROJECT_REPOSITORY,
  useClass: TypeormProjectRepositoryDAO,
};
export type projectRepositoryType = TypeormProjectRepositoryDAO;

export const backlogRepositoryDAO = {
  provide: RepositoryTypes.BACKLOG_REPOSITORY,
  useClass: TypeormBacklogRepositoryDAO,
};
export type backlogRepositoryType = TypeormBacklogRepositoryDAO;

export const userProjectJucntionRepositoryDAO = {
  provide: RepositoryTypes.PROJECT_USER_JUNCTION_REPOSITORY,
  useClass: TypeormUserProjectJunctionRepositoryDAO,
};
export type userProjectJuncitonRepositoryType = TypeormUserProjectJunctionRepositoryDAO;

export const inviteRepositoryDAO = {
  provide: RepositoryTypes.INVITE_REPOSITORY,
  useClass: TypeormInviteRepositoryDAO,
};
export type inviteRepositoryType = TypeormInviteRepositoryDAO;

export const inviteCoherenceRepositoryDAO = {
  provide: RepositoryTypes.INVITE_COHERENCE_REPOSITORY,
  useClass: TypeormInvitesUsersRolesProjectsCoherenceRepositoryDao,
};
export type inviteCoherenceRepositoryType = TypeormInvitesUsersRolesProjectsCoherenceRepositoryDao;
