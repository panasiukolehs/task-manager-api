import { Repository, ObjectType, getRepository } from 'typeorm';
import { Injectable } from '@nestjs/common';

@Injectable()
export abstract class RepositoryDAO<T> {
  protected async _getRepository(entity: ObjectType<T>): Promise<Repository<T>> {
    return getRepository(entity);
  }
}
