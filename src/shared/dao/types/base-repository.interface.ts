export interface IRepository<T> {
  find(): Promise<T[]>;
  findById(id: number): Promise<T>;
}
