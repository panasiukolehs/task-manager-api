import * as bcrypt from 'bcryptjs';

export class EncodingHepler {
  async hashData(data: string, salt: number): Promise<string> {
    return await bcrypt.hash(data, salt);
  }

  async compareData(reqData: string, localData: string): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      bcrypt.compare(reqData, localData, (err, decoded) => {
        if (err) reject(false);
        if (!decoded) reject(false);
        resolve(true);
      });
    });
  }
}
