import { ApiProperty } from '@nestjs/swagger';
import { TaskPriority } from 'src/task/types/priority-type';
import { TaskProgress } from 'src/task/types/progress-type';
import { TaskType } from 'src/task/types/task-type';

export class TaskDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  type: TaskType;

  @ApiProperty()
  name: string;

  @ApiProperty()
  description: string;

  @ApiProperty()
  progress: TaskProgress;

  @ApiProperty()
  priority: TaskPriority;

  @ApiProperty()
  estimatedTimeMs: number;

  @ApiProperty()
  authorId: number;

  @ApiProperty()
  userId: number;

  @ApiProperty()
  sprintId: number;

  @ApiProperty()
  backlogId: number;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
