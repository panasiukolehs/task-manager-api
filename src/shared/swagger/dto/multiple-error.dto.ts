import { ApiProperty } from '@nestjs/swagger';

export class MultipleErrorDto {
  @ApiProperty()
  statusCode: number;

  @ApiProperty()
  message: string[];

  @ApiProperty()
  error: string;
}
