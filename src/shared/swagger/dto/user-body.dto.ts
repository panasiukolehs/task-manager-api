import { ApiProperty } from '@nestjs/swagger';
import { UserIncludeRoleDto, UserIncludeActiveDto } from './user.dto';

export class UserIncludeActiveBodyDto {
  @ApiProperty()
  user: UserIncludeActiveDto;
}

export class UserIncludeRoleBodyDto {
  @ApiProperty()
  user: UserIncludeRoleDto;
}
