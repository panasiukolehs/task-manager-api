import { ApiProperty } from '@nestjs/swagger';

export class PayloadDto {
  @ApiProperty()
  type: string;

  @ApiProperty()
  access_token: string;

  @ApiProperty()
  refresh_token: string;
}
