import { ApiProperty } from '@nestjs/swagger';
import { PayloadDto } from './payload.dto';
import { UserIncludeActiveBodyDto, UserIncludeRoleBodyDto } from './user-body.dto';

export class AuthResponseDto {
  @ApiProperty()
  payload: PayloadDto;
}

export class AuthResponseIncludeActiveDto extends AuthResponseDto {
  @ApiProperty()
  data: UserIncludeActiveBodyDto;
}

export class AuthResponseIncludeRoleDto extends AuthResponseDto {
  @ApiProperty()
  data: UserIncludeRoleBodyDto;
}
