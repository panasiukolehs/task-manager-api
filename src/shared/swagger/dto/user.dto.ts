import { ApiProperty } from '@nestjs/swagger';
import { RoleDto } from './role.dto';

export class UserDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  email: string;
}

export class UserIncludeActiveDto extends UserDto {
  @ApiProperty()
  active: boolean;
}

export class UserIncludeRoleDto extends UserDto {
  @ApiProperty()
  roleId: RoleDto;
}
