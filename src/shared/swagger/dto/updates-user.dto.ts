import { ApiProperty } from '@nestjs/swagger';

export class UpdatesUserDto {
  @ApiProperty()
  email: string;

  @ApiProperty()
  password: string;
}
