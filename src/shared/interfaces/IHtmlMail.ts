export interface IHtmlMail {
  subject: string;
  html: string;
}
