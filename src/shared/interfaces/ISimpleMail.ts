export interface ISimpleMail {
  subject: string;
  text: string;
}
