import { BadRequestException } from '@nestjs/common';

export function generateExpiresDate(): Date {
  const interval = Number(process.env.INVITE_EXPIRATION_IN_DAYS);
  if (Number.isNaN(interval)) {
    throw new BadRequestException('Invalid expiration time constant');
  }
  return new Date(Date.now() + interval * 24 * 60 * 60 * 1000);
}
