export function generateInviteLink(inviteUuid: string, httpProtocol: string): string {
  return `${httpProtocol}://${process.env.HOST}:${process.env.PORT}/invitation/${inviteUuid}`;
}
