import { ITask } from 'src/task/interfaces/ITask';
import { IHtmlMail } from '../interfaces/IHtmlMail';
import { ISimpleMail } from '../interfaces/ISimpleMail';

export class MailTextHelper {
  generateUserAssignNotification(task: ITask): ISimpleMail {
    return {
      subject: 'A new task waits for you',
      text: `Hello, user! You have been assigned to new task - ${task.name} with priority - ${task.priority}.`,
    };
  }

  generateProjectInvitationWithLink(link: string): IHtmlMail {
    return {
      subject: 'Project invitation',
      html:
        `<p>Follow link to join the project on our platform ` +
        `<a href="${link}">accept invitation</a> </p>`,
    };
  }
}
