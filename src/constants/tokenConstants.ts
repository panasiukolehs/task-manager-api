import * as dotenv from 'dotenv';
import * as path from 'path';

const rootDir = path.resolve();
dotenv.config({
  path: path.join(rootDir, '.env'),
});

export const jwtConstant = {
  secret: process.env.JWT_SECRET,
  secretExpiration: process.env.JWT_SECRET_EXPIRATION,
  refreshSecretExpiration: process.env.JWT_REFRESH_SECRET_EXPIRATION,
};
