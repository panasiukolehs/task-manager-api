import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ProjectEntity } from '../../project/entities/project.entity';
import { IProject } from '../../project/interfaces/project-response.interface';
import { IBacklog } from '../interfaces/IBacklog';

@Entity('backlogs')
export class Backlog implements IBacklog {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'created_at' })
  createdAt: Date;

  @Column({ name: 'project_id' })
  projectId: number;

  @Column({ name: 'updated_at' })
  updatedAt: Date;

  @OneToOne(() => ProjectEntity, { cascade: true, onDelete: 'CASCADE' })
  @JoinColumn({ name: 'project_id', referencedColumnName: 'id' })
  project?: IProject;
}
