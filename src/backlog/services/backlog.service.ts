import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { PINO_LOGGER_SERVICE } from '../../constants/serviceConstants';
import { PinoLoggerService } from '../../logger/services/pino-logger.service';
import { backlogRepositoryType } from '../../shared/dao/ConnectionDAO';
import { RepositoryTypes } from '../../shared/dao/types/repository-types.enum';
import { IBacklog } from '../interfaces/IBacklog';
import { IBacklogService } from '../interfaces/IBacklogService';
@Injectable()
export class BacklogService implements IBacklogService {
  constructor(
    @Inject(RepositoryTypes.BACKLOG_REPOSITORY)
    private backlogRepository: backlogRepositoryType,
    @Inject(PINO_LOGGER_SERVICE)
    private readonly logger: PinoLoggerService,
  ) {
    logger.setContext('BacklogService');
  }

  async createBacklog(projectId: number): Promise<IBacklog> {
    return this.backlogRepository.createBacklog(projectId);
  }

  async find(): Promise<IBacklog[]> {
    return this.backlogRepository.find();
  }

  async findById(id: number): Promise<IBacklog> {
    return this.backlogRepository.findById(id);
  }

  async findByProjectId(projectId: number): Promise<IBacklog> {
    const backlog = await this.backlogRepository.findByProjectId(projectId);
    if (!backlog) {
      this.logger.warn(`There is not such a project with id: ${projectId}`);
      throw new NotFoundException(`There is not such a project with id: ${projectId}`);
    }
    return backlog;
  }
}
