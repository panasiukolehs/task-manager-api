import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Backlog } from './entities/backlog.entity';
import { diBacklogService, diLoggerService } from '../DIConfig/serviceConfig';
import { backlogRepositoryDAO } from '../shared/dao/ConnectionDAO';

@Module({
  imports: [TypeOrmModule.forFeature([Backlog])],
  providers: [diBacklogService, backlogRepositoryDAO, diLoggerService],
  exports: [diBacklogService, backlogRepositoryDAO],
})
export class BacklogModule {}
