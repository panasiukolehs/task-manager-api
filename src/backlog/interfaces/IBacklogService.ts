import { IBacklog } from './IBacklog';

export interface IBacklogService {
  createBacklog(projectId: number): Promise<IBacklog>;

  find(): Promise<IBacklog[]>;

  findById(id: number): Promise<IBacklog>;

  findByProjectId(projectId: number): Promise<IBacklog>;
}
