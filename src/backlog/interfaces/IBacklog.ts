import { IProject } from '../../project/interfaces/project-response.interface';

export interface IBacklog {
  id: number;
  projectId: number;
  createdAt: Date;
  updatedAt: Date;
  project?: IProject;
}
