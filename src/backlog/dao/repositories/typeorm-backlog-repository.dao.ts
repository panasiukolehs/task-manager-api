import { Injectable } from '@nestjs/common';
import { DeleteResult } from 'typeorm';
import { RepositoryDAO } from '../../../shared/dao/repositories/typeorm-repository.dao';
import { IRepository } from '../../../shared/dao/types/base-repository.interface';
import { Backlog } from '../../entities/backlog.entity';
import { IBacklog } from '../../interfaces/IBacklog';

@Injectable()
export class TypeormBacklogRepositoryDAO
  extends RepositoryDAO<Backlog>
  implements IRepository<Backlog>
{
  constructor() {
    super();
  }

  async find(): Promise<IBacklog[]> {
    const backlogRepository = await this._getRepository(Backlog);
    return await backlogRepository.find();
  }

  async findById(id: number): Promise<IBacklog> {
    const backlogRepository = await this._getRepository(Backlog);
    return await backlogRepository.findOne(id);
  }

  async findByProjectId(projectId: number): Promise<IBacklog> {
    const backlogRepository = await this._getRepository(Backlog);
    return await backlogRepository
      .createQueryBuilder()
      .where('project_id = :projectId', { projectId })
      .getOne();
  }

  async createBacklog(projectId: number): Promise<IBacklog> {
    const backlogRepository = await this._getRepository(Backlog);
    return await backlogRepository.save({ projectId });
  }

  async deleteBacklog(projectId: number): Promise<DeleteResult> {
    const backlogRepository = await this._getRepository(Backlog);
    const result = backlogRepository
      .createQueryBuilder()
      .delete()
      .where('project_id = :projectId', { projectId })
      .execute();
    return result;
  }
}
