import { ISendMailOptions } from '@nestjs-modules/mailer';
import { SentMessageInfo } from 'nodemailer';
import { IHtmlMail } from 'src/shared/interfaces/IHtmlMail';
import { ISimpleMail } from 'src/shared/interfaces/ISimpleMail';

export interface IMailService {
  notifyUserOnMail(userEmail: string, message: ISimpleMail): Promise<void>;

  notifyUserOnMailWithHtml(userEmail: string, message: IHtmlMail): Promise<boolean>;

  sendCustomMail(mailData: ISendMailOptions): Promise<SentMessageInfo>;
}
