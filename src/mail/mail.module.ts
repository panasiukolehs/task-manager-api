import { MailerModule } from '@nestjs-modules/mailer';
import { Module } from '@nestjs/common';
import mailConfig from 'config/mailConfig';
import { diLoggerService, diMailService } from 'src/DIConfig/serviceConfig';

@Module({
  imports: [MailerModule.forRoot(mailConfig)],
  providers: [diMailService, diLoggerService],
  exports: [diMailService],
})
export class MailModule {}
