import { ISendMailOptions, MailerService } from '@nestjs-modules/mailer';
import { Inject, Injectable } from '@nestjs/common';
import { SentMessageInfo } from 'nodemailer';
import { IHtmlMail } from 'src/shared/interfaces/IHtmlMail';
import { ISimpleMail } from 'src/shared/interfaces/ISimpleMail';
import { PINO_LOGGER_SERVICE } from '../../constants/serviceConstants';
import { PinoLoggerService } from '../../logger/services/pino-logger.service';
import { IMailService } from '../interfaces/IMailService';

@Injectable()
export class MailService implements IMailService {
  constructor(
    private readonly nodemailerService: MailerService,
    @Inject(PINO_LOGGER_SERVICE)
    private readonly logger: PinoLoggerService,
  ) {
    logger.setContext('MailService');
  }

  async notifyUserOnMail(userEmail: string, message: ISimpleMail): Promise<void> {
    try {
      const mailData: ISendMailOptions = {
        to: userEmail,
        from: '"Eclier" <eclier-support@gmail.com>',
        subject: message.subject,
        text: message.text,
      };

      const info = await this.sendCustomMail(mailData);
    } catch (err) {
      this.logger.error(err);
    }
  }

  async notifyUserOnMailWithHtml(userEmail: string, message: IHtmlMail): Promise<boolean> {
    try {
      const mailData: ISendMailOptions = {
        to: userEmail,
        from: '"Eclier" <eclier-support@gmail.com>',
        subject: message.subject,
        html: message.html,
      };

      const info = await this.sendCustomMail(mailData);
      return true;
    } catch (err) {
      this.logger.error(err);
      return false;
    }
  }

  async sendCustomMail(mailData: ISendMailOptions): Promise<SentMessageInfo> {
    return await this.nodemailerService.sendMail(mailData);
  }
}
