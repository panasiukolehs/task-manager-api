import { MailerOptions } from '@nestjs-modules/mailer';
import * as dotenv from 'dotenv';
import * as path from 'path';

const rootDir = path.resolve();
dotenv.config({
  path: path.join(rootDir, '.env'),
});

const mailConfig: MailerOptions = {
  transport: {
    host: 'smtp.gmail.com',
    secure: false,
    auth: {
      user: process.env.APP_MAIL_EMAIL,
      pass: process.env.APP_MAIL_PASSWORD,
    },
  },
  defaults: {
    from: '"No Reply" <noreply@example.com>',
  },
};

export default mailConfig;
