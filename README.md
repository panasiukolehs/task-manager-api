# Task Manager
> This is a training backend project where we implemented the task-manager behavior similar to the Jira application.

### The technologies we use here:

1. npm
2. Docker
3. NestJS
4. Postgres database
5. Typeorm
6. Typescript

### How to run this project:
##### You have to go through these few steps:

* Copy this repository into your computer with this command

    ````
    git clone https://github.com/tanchik-curly/task-manager.git
    ````

* Go to the project directory

    ````
    cd task-manager
    ````

* Switch into dev-branch

	````
    git checkout dev-branch
    ````

* Create **docker.env** and **ormconfig.env** files in the root directory of this project with following variables(this is setting for database):

	* _docker.env example_

		````
        POSTGRES_USER=<your database username>
        POSTGRES_DB=<your database name>
        POSTGRES_PASSWORD=<your database password>
        ````

    * _ormconfig.env example_

    	````
        TYPEORM_HOST=localhost
        TYPEORM_USERNAME=<your database username>
        TYPEORM_PASSWORD=<your database password>
        TYPEORM_DATABASE=<your database name>
        TYPEORM_PORT=5444
        TYPEORM_ENTITIES = dist/src/**/**/*.entity.js
    	````

 	___*The values in the brackets have to be the same in both files.___

* Also create **.env** file in the root directory of this project with following variables (this is setting for project):

    ````
    PORT=<your value>
    HOST=<your value>
  
    #Token Settings
    JWT_SECRET_EXPIRATION=<your value>
    JWT_REFRESH_SECRET_EXPIRATION=<your value>
    JWT_SECRET=<your value>
  
    #Swagger Settings
    APP_NAME=<your value>
    APP_VERSION=<your value>
  
    #Mail Settings
    APP_MAIL_EMAIL=<your gmail>
    APP_MAIL_PASSWORD=<your password>
  
    #Ivite Settings
    INVITE_EXPIRATION_IN_DAYS=<integer only>    
    REGISTRATION_BY_INVITE_LINK=<your link>
    ````
    _*REGISTRATION_BY_INVITE_LINK is a link for redirecting to the front-end page and registration.
     This link musts look like **http://your_path/** or **http://your_path?your_param_name=**
     and be ready to get the UUID value as dinamic parameter or query parameter._
     
> If you haven't installed **docker** yet you must do this. Because you won't be able to execute _**docker compose up**_ command in your terminal for database setup. Then you have to run _**npm install**_ command as well for downloading and configure all dependencies that your project needs. After that run the command _**npm run db:migrate**_ to update database structure by migrations. When you do all this steps you will be able to start the project with _**npm run start:dev**_ command. Enjoy!
