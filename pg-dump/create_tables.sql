CREATE TYPE "task_progress" AS ENUM (
  'todo',
  'in_progress',
  'review',
  'done'
);

CREATE TYPE "task_priority" AS ENUM (
  'low',
  'medium',
  'high',
  'asap'
);

CREATE TYPE "task_type" AS ENUM (
  'task',
  'bug'
);

CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "email" varchar UNIQUE NOT NULL,
  "password" varchar NOT NULL,
  "role_id" int NOT NULL,
  "active" boolean DEFAULT true,
  "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "refresh_tokens" (
  "id" SERIAL PRIMARY KEY,
  "token" varchar,
  "user_id" int NOT NULL,
  "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "roles" (
  "id" SERIAL PRIMARY KEY,
  "role_name" varchar
);

CREATE TABLE "user_project_junction" (
  "user_id" int,
  "project_id" int
);

CREATE TABLE "projects" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar,
  "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "sprints" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar,
  "status" varchar,
  "project_id" int,
  "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "backlogs" (
  "id" SERIAL PRIMARY KEY,
  "project_id" int,
  "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "tasks" (
  "id" SERIAL PRIMARY KEY,
  "type" task_type NOT NULL,
  "name" varchar NOT NULL,
  "description" varchar NOT NULL,
  "progress" task_progress,
  "priority" task_priority,
  "user_id" int,
  "sprint_id" int,
  "backlog_id" int,
  "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE OR REPLACE FUNCTION update_changetimestamp_column()
    RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = now();
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER updated_at_projects BEFORE UPDATE
    ON projects FOR EACH ROW EXECUTE PROCEDURE
    update_changetimestamp_column();

CREATE TRIGGER updated_at_sprints BEFORE UPDATE
    ON sprints FOR EACH ROW EXECUTE PROCEDURE
    update_changetimestamp_column();

CREATE TRIGGER updated_at_users BEFORE UPDATE
    ON users FOR EACH ROW EXECUTE PROCEDURE
    update_changetimestamp_column();

CREATE TRIGGER updated_at_backlogs BEFORE UPDATE
    ON backlogs FOR EACH ROW EXECUTE PROCEDURE
    update_changetimestamp_column();

CREATE TRIGGER updated_at_tasks BEFORE UPDATE
    ON tasks FOR EACH ROW EXECUTE PROCEDURE
    update_changetimestamp_column();


ALTER TABLE "users" ADD FOREIGN KEY ("role_id") REFERENCES "roles" ("id");

ALTER TABLE "refresh_tokens" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "refresh_tokens" ADD CONSTRAINT "UQ_user_id" UNIQUE ("user_id");

ALTER TABLE "user_project_junction" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "user_project_junction" ADD FOREIGN KEY ("project_id") REFERENCES "projects" ("id");

ALTER TABLE "user_project_junction" ADD PRIMARY KEY ("project_id", "user_id");

ALTER TABLE "sprints" ADD FOREIGN KEY ("project_id") REFERENCES "projects" ("id");

ALTER TABLE "backlogs" ADD FOREIGN KEY ("project_id") REFERENCES "projects" ("id");

ALTER TABLE "tasks" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "tasks" ADD FOREIGN KEY ("sprint_id") REFERENCES "sprints" ("id");

ALTER TABLE "tasks" ADD FOREIGN KEY ("backlog_id") REFERENCES "backlogs" ("id");

CREATE OR REPLACE FUNCTION update_changetimestamp_column()
    RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = now();
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER updated_at_tokens BEFORE UPDATE
    ON refresh_tokens FOR EACH ROW EXECUTE PROCEDURE
    update_changetimestamp_column();